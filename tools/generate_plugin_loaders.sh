lua tools/generate_plugin_loader.lua DocumentAutoParser tasks resource document_store > src/page/plugin_loader/document_auto_parser.vala
lua tools/generate_plugin_loader.lua RedirectDialog tasks internal_navigation transaction_log dialog_ui > src/page/plugin_loader/redirect_dialog.vala
lua tools/generate_plugin_loader.lua Autoredirect tasks internal_navigation transaction_log > src/page/plugin_loader/autoredirect.vala
lua tools/generate_plugin_loader.lua GeminiInputToDocument tasks internal_navigation transaction_log document_store > src/page/plugin_loader/gemini_input_to_document.vala

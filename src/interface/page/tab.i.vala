public interface Slate.Interface.Page.Tab : Object {
	
	public signal void current_page_changed();
	
	public abstract Slate.Interface.Page.Page get_current_page();
	
	public abstract Slate.Interface.Page.Service.ExternalNavigation get_navigation();
	public abstract Slate.Interface.Page.Service.LinearHistory? get_history();
	
	public abstract Slate.Interface.Page.Service.Containers? get_continers_service();
	public abstract Slate.Interface.Page.Container? get_own_container();
	public abstract string? get_own_container_id();
	
}

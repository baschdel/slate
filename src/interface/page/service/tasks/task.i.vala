public interface Slate.Interface.Page.Service.Tasks.Task : Object {
	
	/*
		This interface is supposed to be implemented on all objects that …
		* … carry out ONE action
		* … carry out an action that takes some time
		* … provide actions the user might be intered in manually activating (like cancelling or pausing the action)
		* … are passive listeners on page services most of the time and don't provide a service themselves
		
		Examples of tasks are:
		* Downloading a file
		* Converting a file
		* Parsing a file
		* Basically anything with files
		* Updating metadata based on the current state of transactions
		
	*/
	
	public signal void on_progress_updated(Slate.Interface.Page.Service.Tasks.Task task);
	public signal void on_actions_updated(Slate.Interface.Page.Service.Tasks.Task task);
	public signal void on_task_state_updated(Slate.Interface.Page.Service.Tasks.Task task);
	
	public abstract string get_task_name();
	public abstract string get_task_category();
	public abstract double? get_task_progress();
	public abstract string get_task_state();
	public abstract bool has_task_finished();
	public virtual string[] get_available_task_actions(){ return {}; }
	public virtual bool activate_task_action(string action){ return false; }
	public abstract void foreach_task_property(Func<string> cb);
	public abstract string? read_task_property(string key);
	
}

public interface Slate.Interface.Page.Service.Tasks.Service : Object {
	
	/*
		The task service should keep track of long running tasks.
		One of its jobs is to prevent objects, that are just listening to other page services
		from being garbage collected.
		
		It also provides an overview wich tasks are currently running and a way to cancel them
		
		If you add a task it will keep the reference for a very long time after the task has finished to make it easier for logging and monitoring services.
	*/
	
	public signal void task_added(string task_id, Slate.Interface.Page.Service.Tasks.Task task);
	
	public abstract string add_task_object(Slate.Interface.Page.Service.Tasks.Task task); //returns the task id
	public abstract Slate.Interface.Page.Service.Tasks.Task? get_task_object(string task_id);
	public abstract void foreach_registred_task(HFunc<string, Slate.Interface.Page.Service.Tasks.Task> cb, bool only_ongoing = false);
	
}

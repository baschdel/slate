public interface Slate.Interface.Page.Service.Log.Service : Object {
	
	public signal void entry_submitted(Slate.Interface.Page.Service.Log.Entry entry, uint64 num);

	public abstract void submit_entry(Slate.Interface.Page.Service.Log.Entry entry);
	
	public abstract uint64 get_number_of_submitted_entrys();
	public abstract Slate.Interface.Page.Service.Log.Entry? get_nth_entry(uint64 num);
	
}

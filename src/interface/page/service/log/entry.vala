public class Slate.Interface.Page.Service.Log.Entry : Object {
	public string module_name { get; private set; }
	public string system_name { get; private set; }
	public string log_entry_type { get; private set; }
	public string[] args { get; private set; }
	public string message { get; private set; }
	public string channel { get; private set; }
	public uint64 timestamp; //number of milliseconds since 1970-01-01 UTC
	
	public Entry(string module_name, string system_name, string log_entry_type, string [] args, string message, string channel){
		this.module_name = module_name;
		this.system_name = system_name;
		this.log_entry_type = log_entry_type;
		this.args = args;
		this.message = message;
		this.channel = channel;
		this.timestamp = (GLib.get_real_time()/1000);
	}
}

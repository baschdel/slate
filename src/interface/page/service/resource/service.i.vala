public interface Slate.Interface.Page.Service.Resource.Service : Object {
	
	//if the resource is null the resource was removed
	public signal void resource_updated(string resource_name, Slate.Interface.Page.Service.Resource.Resource? resource);
	
	public abstract bool is_resource_avaible(string resource_name);
	public abstract void foreach_resource(HFunc<string,Slate.Interface.Page.Service.Resource.Resource> cb);
	
	public abstract void remove_resource(string resource_name);
	public abstract void set_resource(string resource_name, Slate.Interface.Page.Service.Resource.Resource resource);
	
	public abstract Slate.Interface.Page.Service.Resource.Resource? get_resource(string resource_name);
	
	
}

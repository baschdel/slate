public class Slate.Interface.Page.Service.Transaction.LogEntry : Object {
	/*
		Log entrys have the following fields:
			string topic - The general category the message falls in e.g. error, resource, message, redirect, info, success
			string key - what this log entry is about
			string protocol - which protocol generated this message
			string handler - who generated this message (use classname)
			string? statuscode - a standardized statuscode (e.g. http, ftp or gemini status codes)
			string? message - a human readable message (from the server)
			string? uri - if it makes sense to log an uri (for redirects) store it in here
			string? data - can be used to store apropriate data, that does not fit in any existing field, i.e. a resource id
			uint64 timestamp - when the log entry was submitted
	*/
	
	public string topic { get; private set; }
	public string key { get; private set; }
	public string protocol { get; private set; }
	public string handler { get; private set; }
	public string? statuscode { get; private set; }
	public string? message { get; private set; }
	public string? uri { get; private set; }
	public string? data { get; private set; }
	public uint64 timestamp; //number of milliseconds since 1970-01-01 UTC
	
	public LogEntry(string topic, string key, string protocol, string handler, string? statuscode, string? message, string? uri = null, string? data = null){
		this.topic = topic;
		this.key = key;
		this.protocol = protocol;
		this.handler = handler;
		this.statuscode = statuscode;
		this.message = message;
		this.uri = uri;
		this.data = data;
		this.timestamp = (GLib.get_real_time()/1000);
	}
}

public class Slate.Interface.Page.Service.Transaction.Resource : Object {

	// If you are not the transaction log only read the public proprtys
	// Do not call any non-getters
	
	public Slate.Interface.Page.Service.Resource.Resource? resource = null;
	public string transaction_id;
	public string uri;
	public uint64? final_size;
	public uint64 current_size = 0;
	public double? current_speed = null;
	public double? average_speed = null;
	public bool is_finished = false;
	
	private uint64 start_timestamp;
	private uint64 last_timestamp;
	
	public Resource(Slate.Interface.Page.Service.Resource.Resource? resource, string transaction_id, string uri, uint64? final_size){
		this.resource = resource;
		this.transaction_id = transaction_id;
		this.uri = uri;
		this.final_size = final_size;
		this.start_timestamp = (GLib.get_monotonic_time()/1000);
		this.last_timestamp = (GLib.get_monotonic_time()/1000);
	}
	
	public bool add_resource(Slate.Interface.Page.Service.Resource.Resource resource){
		if (!is_finished) {
			lock(this.resource){
				if (this.resource == null) {
					this.resource = resource;
					return true;
				}
			}
		}
		return false;
	}
	
	public bool update_size(uint64 current_size, uint64? final_size = null){
		if (!is_finished) {
			if (final_size != null) {
				this.final_size = final_size;
			}
			uint64 now = (GLib.get_monotonic_time()/1000);
			if (now > this.start_timestamp) {
				this.average_speed = (current_size/(now-this.start_timestamp))*1000;
			}
			if (now > this.last_timestamp) {
				this.current_speed = ((current_size-this.current_size)/(now-this.last_timestamp))*1000;
			}
			this.current_size = current_size;
			this.last_timestamp = now;
			return true;
		} else {
			return false;
		}
	}
	
	public bool finish(bool is_complete){
		if (is_finished) { return false; }
		if (is_complete) {
			this.final_size = this.current_size;
		}
		this.is_finished = true;
		return true;
	}
	
}

public interface Slate.Interface.Page.Service.Transaction.Log : Object {
	
	/*
		This interface provides an API to monitor loading processes started with a call to the load interfacae,
		but should be able to handle other, more specialized APIs too.
		This interface isorganized in transactions
		Each transaction has resources and a log associated with it
	*/
	
	/*
		Log entrys have the following fields:
			string topic - The general category the message falls in e.g. error, resource, message, redirect, info, success
			string key - what this log entry is about
			string protocol - which protocol generated this message
			string handler - who generated this message (use classname)
			string? statuscode - a standardized statuscode (e.g. http, ftp or gemini status codes)
			string? message - a human readable message (from the server)
			string? uri - if it makes sense to log an uri (for redirects) store it in here
			string? data - can be used to store apropriate data, that does not fit in any existing field, i.e. a resource id
			uint64 timestamp - when the log entry was submitted
			
		Planned:
			An extension of thi api to manage transaction actions (like pausing, cancelling or requesting more/another source(s))
			
	/*
		Resources:
		
		Plese don't get confused, currently there are three kinds of resources in Slate:
		* Slate.Resource - This one is specific to the old slate backend
		* Slate.Interface.Page.Service.Resource.Resource - This one is used by the page system, it can provide data streams and immutable metadata
		* Slate.Interface.Page.Service.Transaction.Resource - This one is used by this system to provide mutable metadata on load status etc. along a page resource
		
		It's a bit like Aperture Science many layers, one above the other the lower ones a bit obscure, all of them a bit confusing but it somehow works.
		Minus the getting destroyed or flooded part.
	*/
	
	// listening
	
	public signal void transaction_started(string transaction_id, string uri, string verb, Slate.Interface.Page.Service.Tasks.Task? task);
	public signal void transaction_ended(string transaction_id);
	public signal void log_entry_submitted(string transaction_id, Slate.Interface.Page.Service.Transaction.LogEntry entry);
	
	// Read transaction ids from resource
	//resource added is triggered when the actual backing resource was added
	//you may see resource_updated signals before the resource_added signal or no resource_added signal at all
	public signal void resource_added(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource);
	public signal void resource_updated(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource);
	public signal void resource_finished(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource);
	
	// reading
	
	public abstract bool has_transaction(string transaction_id);
	public abstract string? get_transaction_uri(string transaction_id);
	public abstract string? get_transaction_verb(string transaction_id);
	public abstract Slate.Interface.Page.Service.Tasks.Task? get_transaction_task(string transaction_id);
	public abstract Slate.Interface.Page.Service.Transaction.LogEntry? get_transaction_state(string transaction_id); //the latest "state" log entry (guranteed to be a state update)
	public abstract bool? has_transaction_ended(string transaction_id);
	public abstract void foreach_transaction_log_entry(string transaction_id, Func<Slate.Interface.Page.Service.Transaction.LogEntry> cb);
	public abstract void foreach_transaction(Func<string> cb);
	
	public abstract void foreach_resource(string transaction_id, Func<string> cb);
	public abstract bool has_resource(string resource_id);
	public abstract Slate.Interface.Page.Service.Transaction.Resource? get_resource(string resource_id);
	
	// writing
	public abstract string start_transaction(string uri, string verb = "download", Slate.Interface.Page.Service.Tasks.Task? task = null); // returns the transaction id
	public abstract bool end_transaction(string transaction_id);
	public abstract bool submit_log_entry(string transaction_id, Slate.Interface.Page.Service.Transaction.LogEntry entry);
	
	// download resources - for resources, that were downloaded, no matter the verb
	public abstract string? submit_resource(string transaction_id, Slate.Interface.Page.Service.Resource.Resource? resource, string uri, uint64? final_size); //returns a resource id
	public abstract bool add_resource(string resource_id, Slate.Interface.Page.Service.Resource.Resource resource); //add a page resource (can't change an already set resource)
	public abstract bool update_resource_size(string resource_id, uint64 size, uint64? final_size = null);
	public abstract bool finish_resource(string resource_id, bool is_complete);
	
	
	
}

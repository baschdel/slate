public interface Slate.Interface.Page.Service.Containers : Object {
	
	/*
		This service exists to query containers, when opening a new tab you have to answer three questions:
		* What to display?
		* Where to display it?
		* How to load it?
		This one will help answering the how to load it question.
		Functions that will result in the creation of a new tab will ask for a container,
		This interface will help you in getting access to a container by its id.
		The Policy of wich container is recommended is left to a seperate service, as this
		* is dependant on the context and
		* should be easy to implement without having to worry about managing containers
	*/
	
	public signal void container_updated(string container_id);
	
	public abstract void set_container(string container_id, Slate.Interface.Page.Container? container);
	
	public abstract Slate.Interface.Page.Container? get_contatiner(string container_id);
	public abstract bool has_container(string container_id);
	// will iterate over all containers, if an uri is specified, only over those who claim to be able to crate a tab for the uri.
	public abstract void foreach_container(Func<string> cb, string? uri = null);
}

public interface Slate.Interface.Page.Service.PagePlugin.Index : Object {
	
	/*
		Objects implementing this interface can be used as a page service.
		They provide a list of plugins that can be loaded either when the page gets initialized,
		or when they are needed.
	*/
	
	public abstract Slate.Interface.Page.Service.PagePlugin.Loader? get_plugin_loader(string id);
	public abstract void foreach_plugin_loader(Func<Slate.Interface.Page.Service.PagePlugin.Loader> cb);
	
}

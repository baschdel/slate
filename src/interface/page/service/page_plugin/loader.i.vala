public interface Slate.Interface.Page.Service.PagePlugin.Loader : Object {
	
	/*
		A loader is an object that can load exactly one type of page_plugin.
		Loaders are used together with a loader index to make it possible to have a lit of loadable plugins.
		Loaders can load plugins, test if they can be loaded and test if they are already loaded.
		They also provide a plugin_id wich can be used to derive plugin name and description from the localisation subsystem.
		Usually the keys are derived using the following patterns:
			page_plugin.<plugin_id>.name
			page_plugin.<plugin_id>.description
	*/	
	
	public abstract string get_plugin_id();
	public abstract string get_plugin_version();
	
	public abstract bool can_load_plugin(Slate.Interface.Page.Page page);
	public abstract bool is_plugin_loaded(Slate.Interface.Page.Page page);
	
	public abstract bool load_plugin(Slate.Interface.Page.Page page);
	
}

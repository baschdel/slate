public interface Slate.Interface.Ui.DialogAction : Object {
	/*
		This interface is used together with the dialog interface,
		It provides actions as an all in one object
		
		Each action has a name, wich can be used to activate it.
		Each action may optionally provide a uri along the action name
		to make it possible to have menus like open uri in new tab/window/different container
		
		The action name will be used to derive the localized label and the icon of an action
		slate.dialog.<dialog_name>.action.<action_name>.label
		slate.dialog.<dialog_name>.action.<action_name>.tooltip_text
	*/
	
	public abstract string get_action_name();
	public virtual string? get_action_uri(){ return null; }
	public virtual bool is_action_destructive(){ return false; }
	
	public abstract bool activate_action();
}

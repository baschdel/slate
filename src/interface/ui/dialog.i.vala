public interface Slate.Interface.Ui.Dialog : Object {

	/*
		This interface is intended for dialogs like youd see when:
		* The Page is asking for a redirect
		* The Page load was cancelled
		* The Page load resulted in an error
		
		The dialog name is a name for internal use.
		It is used to derive the dialog title,
		an optional subtitle and the dialog icon
		
		The Title and subtitle are supposed to be fetched from the translation using the following templates:
		slate.dialog.<dialog_name>.title
		slate.dialog.<dialog_name>.subtitle
		
		Icons will be derived using a frontend specific mechanism
		
		A dialog may have a message, wich should be a message from the server or debugging information
		Make sure when displaying the message to add a way to copy it
		
		A dialog may also have actions associated with it.
		For more documentation on actions see Slate.Interface.Ui.DialogAction
		
		if the has_primary_action flag is set the first action in the actions array should be highlighted
	*/
	
	public abstract string get_dialog_name();
	public abstract string? get_dialog_message();
	
	public abstract bool has_primary_action();
	public abstract Slate.Interface.Ui.DialogAction[] get_actions();
	
}

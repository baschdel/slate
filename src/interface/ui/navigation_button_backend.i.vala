public interface Slate.Interface.Ui.NavigationButtonBackend : Object {
	
	public abstract void go();
	public abstract bool can_go();
	public abstract string? get_primary_location();
	
}

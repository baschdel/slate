public interface Slate.Interface.Core : Object {
	
	/*
		Objects implementing this interface are providing, as the name suggests core functionality for slate.
		This is to keep all non UI code out of the UI initialization and to make it easier to build a different frontend.
		The cores main duties are reading the configuration and setting up subsystems that are then given to tabs, pages and page-services.
	*/
	
	public abstract Slate.Interface.Settings.Provider get_root_settings_provider();
	public abstract Slate.Interface.Page.Service.Containers get_containers_service();
	public abstract Slate.Interface.Page.Service.PagePlugin.Index get_page_plugin_index();
	
}

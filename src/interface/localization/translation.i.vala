public interface Slate.Interface.Localization.Translation : Object {

	//return the text_id prefixed with a ?_ if no translation is avaiable
	public abstract string get_localized_string(string text_id);
	
}

public enum Slate.Ui.TabDisplayState {
	LOADING,
	ERROR,
	CONTENT,
	BLANK;
}

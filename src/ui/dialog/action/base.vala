public abstract class Slate.Ui.Dialog.Action.Base : Slate.Interface.Ui.DialogAction, Object {
	
	protected string? uri = null;
	protected string name = "dummy";
	protected bool destructive = false;
	
	  /////////////////////////////////////
	 // Slate.Interface.Ui.DialogAction //
	/////////////////////////////////////
	
	public virtual string get_action_name(){
		return this.name;
	}
	
	public virtual string? get_action_uri(){
		return this.uri;
	}
	
	public virtual bool is_action_destructive(){
		return this.destructive;
	}
	
	public abstract bool activate_action();
	
}

public class Slate.Ui.Dialog.Action.Uri : Slate.Interface.Ui.DialogAction, Slate.Ui.Dialog.Action.Base {
	
	private Slate.Interface.Page.Service.ExternalNavigation external_navigation;
	private string module_name;
	
	public Uri(string uri, Slate.Interface.Page.Service.ExternalNavigation external_navigation, string module_name, string name = "navigate", bool destructive = false){
		this.uri = uri;
		this.external_navigation = external_navigation;
		this.module_name = module_name;
		this.name = name;
		this.destructive = destructive;
	}
	
	  /////////////////////////////////////
	 // Slate.Interface.Ui.DialogAction //
	/////////////////////////////////////
	
	public override bool activate_action(){
		external_navigation.go_to_uri(this.uri, module_name);
		return true;
	}
	
}

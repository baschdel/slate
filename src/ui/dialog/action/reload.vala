public class Slate.Ui.Dialog.Action.Reload : Slate.Interface.Ui.DialogAction, Slate.Ui.Dialog.Action.Base {
	
	private Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	
	public Reload(Slate.Interface.Page.Service.InternalNavigation internal_navigation, string name = "reload", bool destructive = false){
		this.internal_navigation = internal_navigation;
		this.name = name;
		this.destructive = destructive;
	}
	
	  /////////////////////////////////////
	 // Slate.Interface.Ui.DialogAction //
	/////////////////////////////////////
	
	public override bool activate_action(){
		internal_navigation.reload();
		return true;
	}
	
}

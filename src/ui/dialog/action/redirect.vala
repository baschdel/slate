public class Slate.Ui.Dialog.Action.Redirect : Slate.Interface.Ui.DialogAction, Slate.Ui.Dialog.Action.Base {
	
	private Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	private string module_name;
	
	public Redirect(string uri, Slate.Interface.Page.Service.InternalNavigation internal_navigation, string module_name, string name = "redirect", bool destructive = false){
		this.uri = uri;
		this.internal_navigation = internal_navigation;
		this.module_name = module_name;
		this.name = name;
		this.destructive = destructive;
	}
	
	  /////////////////////////////////////
	 // Slate.Interface.Ui.DialogAction //
	/////////////////////////////////////
	
	public override bool activate_action(){
		internal_navigation.redirect(this.uri, module_name);
		return true;
	}
	
}

public class Slate.Ui.LoadingWidgetBackend.WatchTransaction : Object, Slate.Interface.Ui.LoadingWidgetBackend {
	
	private Slate.Interface.Page.Page? _page = null;
	private Slate.Interface.Page.Service.Transaction.Log? transaction_log = null;
	
	//settings
	private bool track_by_transaction_id = false; //track by uri if null
	private string? track_uri = null; //use uri from page internal navigation if null
	
	private string? current_uri = null;
	private string? current_transaction_id = null;
	private string? current_resource_id = null;
	private Slate.Interface.Page.Service.Tasks.Task? current_task = null;
	private Slate.Interface.Page.Service.Transaction.Resource? resource = null;
	
	public Slate.Interface.Page.Page? page {
		set {
			if (transaction_log != null) {
				transaction_log.transaction_started.disconnect(on_transaction_started);
				transaction_log.transaction_ended.disconnect(on_transaction_ended);
				transaction_log.log_entry_submitted.disconnect(on_log_entry_submitted);
				transaction_log.resource_updated.disconnect(on_resource_updated);
				transaction_log.resource_finished.disconnect(on_resource_updated);
			}
			if (_page != null) {
				_page.get_internal_navigation_service().current_uri_changed.disconnect(on_uri_changed);
			}
			_page = value;
			transaction_ended = false;
			transaction_state = null;
			if (_page != null) {
				this.transaction_log = _page.get_transaction_log_service();
				var internal_navigation = _page.get_internal_navigation_service();
				internal_navigation.current_uri_changed.connect(on_uri_changed);
				this.on_uri_changed(internal_navigation.get_current_uri());
			} else {
				on_uri_changed(null);
			}
			if (transaction_log != null) {
				transaction_log.transaction_started.connect(on_transaction_started);
				transaction_log.transaction_ended.connect(on_transaction_ended);
				transaction_log.log_entry_submitted.connect(on_log_entry_submitted);
				transaction_log.resource_updated.connect(on_resource_updated);
				transaction_log.resource_finished.connect(on_resource_updated);
			}
		}
		get {
			return _page;
		}
	}
	
	//keep track of state
	private bool transaction_ended = false;
	private string? transaction_state = null;
	
	private void on_uri_changed(string? uri) {
		if (track_uri == null) {
			set_uri(uri);
		}
	}
	
	private void on_transaction_started(string transaction_id, string uri, string verb) {
		if (!track_by_transaction_id) {
			if (uri == this.track_uri || track_uri == null) {
				set_transaction_id(transaction_id);
			}
		}
	}
	
	private void on_transaction_ended(string transaction_id){
		if (transaction_id == this.current_transaction_id) {
			this.transaction_ended = true;
			this.progress_updated();
		}
	}
	
	private void on_log_entry_submitted(string transaction_id, Slate.Interface.Page.Service.Transaction.LogEntry entry){
		if (transaction_id == current_transaction_id){
			if (entry.topic == "state") {
				set_transaction_state(entry.key);
			}
		}
	}
	
	private void on_resource_updated(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource){
		if (current_resource_id == null) {
			if (resource.uri == this.track_uri || track_uri == null) {
				set_resource_id(resource_id);
				this.resource = resource;
			}
		}
		if (this.current_resource_id == resource_id) {
			if (resource.is_finished) {
				current_resource_id = null;
			}
			this.progress_updated();
		}
	}
	
	private void set_uri(string? uri) {
		bool emit_signal = (this.current_uri != uri);
		this.current_uri = uri;
		if (emit_signal) {
			address_updated(uri);
		}
		if (transaction_log != null) {
			string? new_transaction_id = null;
			transaction_log.foreach_transaction((transaction_id) => {
				if (this.current_uri == transaction_log.get_transaction_uri(transaction_id)) {
					new_transaction_id = transaction_id;
				}
			});
			set_transaction_id(new_transaction_id);
		}
	}
	
	private void set_resource_id(string? resource_id){
		bool emit_signal = (this.current_resource_id != resource_id);
		this.current_resource_id = resource_id;
		if (emit_signal) {
			address_updated(resource_id);
			this.resource = null;
		}
	}
	
	private void set_transaction_id(string? transaction_id){
		print(@"Setting transaction id $(transaction_id != null)\n");
		set_resource_id(null);
		transaction_ended = false;
		transaction_state = null;
		bool emit_signal = (this.current_transaction_id != transaction_id);
		this.current_transaction_id = transaction_id;
		lock (this.current_task) {
			if (transaction_id != null) {
				this.current_task = transaction_log.get_transaction_task(transaction_id);
			} else {
				this.current_task = null;
			}
		}
		if (transaction_id != null && transaction_log != null) {
			if(transaction_log.has_transaction_ended(transaction_id) == true) {
				this.on_transaction_ended(transaction_id);
			}
			var transaction_state_update = transaction_log.get_transaction_state(transaction_id);
			if (transaction_state_update != null) {
				set_transaction_state(transaction_state_update.key);
			} else {
				transaction_state = null;
			}
		}
		if (emit_signal) {
			transaction_updated(transaction_id);
		}
	}
	
	private void set_transaction_state(string state) {
		bool emit_signal = transaction_state != state;
		transaction_state = state;
		print(@"Transaction state $transaction_state emit signal: $emit_signal\n");
		if (emit_signal) {
			this.progress_updated();
		}
	}
	
	 ////////////////////////////////
	// Use these to set the target
	
	public void set_target(string? transaction_id, string? uri = null){
		track_by_transaction_id = transaction_id != null;
		if (track_by_transaction_id) {
			set_transaction_id(transaction_id);
		}
		track_uri = uri;
		set_uri(uri);
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Ui.LoadingWidgetBackend //
	/////////////////////////////////////////////
	
	public string? get_current_address(){
		return this.current_uri;
	}
	
	public string? get_transaction_id(){
		return this.current_transaction_id;
	}
	
	public string? get_resource_id(){
		return this.current_resource_id;
	}
	
	public double? get_overall_progress(){
		if (resource != null) {
			if (resource.final_size != null && resource.final_size != 0) {
				return resource.current_size/resource.final_size;
			}
		}
		return null;
	}
	
	public bool is_connecting(){
		return transaction_state == "connecting" && !transaction_ended;
	}
	
	public bool is_shaking_hands(){
		return (transaction_state == "shaking_hands") && !is_downloading() && !transaction_ended;
	}
	
	public bool has_transaction_ended(){
		return transaction_ended;
	}
	
	public bool is_downloading(){
		if (resource != null && !transaction_ended) {
			return !resource.is_finished;
		}
		return false;
	}
	
	public uint64 get_bytes_downloaded(){
		if (resource != null) {
			return resource.current_size;
		}
		return 0;
	}
	
	public uint64? get_expected_bytes_downloaded(){
		if (resource != null) {
			return resource.final_size;
		}
		return null;
	}
	
	public double? get_current_download_speed(){
		if (resource != null) {
			return resource.current_speed;
		}
		return null;
	}
	
	public double? get_average_download_speed(){
		if (resource != null) {
			return resource.average_speed;
		}
		return null;
	}
	
	public bool trigger_action(string action){
		lock (this.current_task) {
			if (this.current_task != null) {
				return this.current_task.activate_task_action(action);
			} else {
				return false;
			}
		}
	}
	
	public string[] get_available_actions(){
		lock (this.current_task) {
			if (this.current_task != null) {
				return this.current_task.get_available_task_actions();
			} else {
				return {};
			}
		}
	}
	
}

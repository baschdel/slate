public class Slate.Ui.AddressbarBackend.MainAddressbar : Object, Slate.Interface.Ui.AddressbarBackend {
	
	// View related
	private Slate.Interface.Page.Page? _page = null;
	
	private string? current_address = null;
	
	public Slate.GtkUi.Interface.ViewContext? view_context = null;
	public string module_name = "main_addressbar_backend";
	
	public Slate.Interface.Page.Page? page {
		set {
			if (_page != null) {
				_page.get_internal_navigation_service().current_uri_changed.disconnect(on_uri_changed);
			}
			_page = value;
			if (_page != null) {
				var internal_navigation = _page.get_internal_navigation_service();
				internal_navigation.current_uri_changed.connect(on_uri_changed);
				this.on_uri_changed(internal_navigation.get_current_uri());
			} else {
				on_uri_changed(null);
			}
		}
		get {
			return _page;
		}
	}
	
	private void on_uri_changed(string? uri){
		this.current_address = uri;
		this.address_updated(uri);
	}
	
	private bool is_reload_address(string address){
		return page != null && (address == "." || address == current_address);
	}
	
	  ////////////////////////////////////////////////
	 // Slate.Interface.Ui.AddressbarBackend //
	////////////////////////////////////////////////
	
	public string? get_current_address(){
		if (_page != null) {
			current_address = _page.get_internal_navigation_service().get_current_uri();
			return current_address;
		} else {
			current_address = null;
			return null;
		}
	}
	
	public string get_action_icon_for_address(string address){
		if (is_reload_address(address)) {
			return "reload";
		} else {
			return "go";
		}
	}
	
	public void submit_address(string address){
		if (_page != null) {
			if (is_reload_address(address)) {
				_page.get_internal_navigation_service().reload();
			} else {
				_page.get_external_navigation_service().go_to_uri(address, module_name);
			}
		} else if (view_context != null){
			view_context.open_uri_in_new_tab(address, null, null);
		}
	}
	
	public string try_address_correction(string address){
		string input = address.strip();
		// replace a search term with a search uri
		if (input.contains(" ")){
			string? bang = null;
			string searchterm = "";
			foreach(string token in input.split(" ")){
				if (token != "") {
					if (token.has_prefix("!")) {
						if (bang == null) {
							bang = token;
						}
					} else {
						if (searchterm != "") {
							searchterm = searchterm+" ";
						}
						searchterm = searchterm+token;
					}
				}
			}
			switch (bang) {
				case "!gsi":
					return "gemini://geminispace.info/search?"+Uri.escape_string(searchterm);
				case "!medusae":
					return "gemini://medusae.space/search_all.gmi?"+Uri.escape_string(searchterm);
			}
		}
		//
		if (!input.has_prefix("gemini://")) {
			if (input.has_prefix("gemini:")) {
				return "gemini://"+input.substring(7);
			}
			if (input.has_prefix("gemini.")) {
				return "gemini://"+input;
			}
		}
		return input;
	}
	
}

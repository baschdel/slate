public class Slate.Ui.NavigationButtonBackend.LinearHistoryBack : Object, Slate.Interface.Ui.NavigationButtonBackend {

	// View related
	public Slate.Interface.Page.Page? page = null;
	
	  ////////////////////////////////////////////////
	 // Slate.Interface.Ui.NavigationButtonBackend //
	////////////////////////////////////////////////
	
	public void go(){
		if (page != null) {
			var linear_history = page.get_linear_history_service();
			if (linear_history != null) {
				linear_history.go_back();
			}
		}
	}
	
	public bool can_go(){
		if (page != null) {
			var linear_history = page.get_linear_history_service();
			if (linear_history != null) {
				return linear_history.can_go_back();
			}
		}
		return false;
	}
	
	public string? get_primary_location(){
		if (page != null) {
			var linear_history = page.get_linear_history_service();
			if (linear_history != null) {
				return linear_history.get_next_past_uri();
			}
		}
		return null;
	}
	
}

public class Slate.Core.Default : Slate.Interface.Core, Object {
	
	/*
		This is the current default implementation of the slate application core.
		While not much and not very flexible, it is at least a place to start.
	*/
	
	private Slate.Interface.Settings.Provider root_settings_provider;
	private Slate.Interface.Page.Service.Containers containers_service;
	private Slate.Interface.Page.Service.PagePlugin.Index page_plugin_index;
	
	public Default(){
		root_settings_provider = Slate.Init.Settings.Backend.get_settings_tree();
		page_plugin_index = Slate.Init.PagePluginIndex.get_plugin_index();
		containers_service = new Slate.Page.Service.Containers();
		containers_service.set_container("default", Slate.Init.Container.Dragonstone.get_smallnet_container(page_plugin_index));
		containers_service.set_container("files", Slate.Init.Container.Dragonstone.get_file_container(page_plugin_index));
	}
	
	  //////////////////////////
	 // Slate.Interface.Core //
	//////////////////////////
	
	public Slate.Interface.Settings.Provider get_root_settings_provider(){
		return root_settings_provider;
	}
	
	public Slate.Interface.Page.Service.Containers get_containers_service(){
		return containers_service;
	}
	
	public Slate.Interface.Page.Service.PagePlugin.Index get_page_plugin_index(){
		return page_plugin_index;
	}
	
}

public class Slate.Init.Localization.English {
	
	public static Slate.Interface.Localization.Translation get_translation(){
		var translation = new Slate.Localization.Translation.Map();
		
		// General purpose actions
		translation.set_translation("action.cancel","Cancel");
		
		// Tab header bar
		translation.set_translation("slate.tab_header.close_button.tooltip_text", "Close Tab");
		
		// Header bar
		//// History Navigation
		translation.set_translation("slate.history.go_back_button.tooltip_text", "Go back in history to");
		translation.set_translation("slate.history.go_forward_button.tooltip_text", "Go forward in history to");
		
		//// Addressbar
		translation.set_translation("slate.main_address_entry.reload_button.tooltip_text", "Reload current page");
		translation.set_translation("slate.main_address_entry.go_button.tooltip_text", "Go to address in address bar");
		
		// Views
		//// Simple Loading
		translation.set_translation("slate.simple_loading.connecting", "Connecting …");
		translation.set_translation("slate.simple_loading.downloading", "Downloading");
		
		// Dialogs
		//// Redirect
		translation.set_translation("slate.dialog.redirect.title", "REDIRECT");
		translation.set_translation("slate.dialog.redirect.subtitle", "Going somewhere else …");
		translation.set_translation("slate.dialog.redirect.action.redirect.tooltip_text", "Follow Redirect");
		
		return translation;
	}
	
}

public class Slate.Init.Settings.Backend {
	
	public static Slate.Interface.Settings.Provider? get_file_settings_provider(string subdirectory, string prefix, string name){
		string settingsdir = GLib.Environment.get_user_config_dir();
		settingsdir = settingsdir+"/slate/"+subdirectory;
		GLib.DirUtils.create_with_parents(settingsdir,16832);
		var provider = new Slate.Settings.FileProvider(settingsdir, name, prefix);
		return provider;
	}
	
	
	public static Slate.Interface.Settings.Provider get_settings_tree(){
		var settings_fallback = new Slate.Settings.Context.Fallback();
		settings_fallback.add_fallback(Slate.Init.Settings.Backend.get_file_settings_provider("", "", "root_settings_provider"));
		//Add more providers here
		return settings_fallback;
	}
}

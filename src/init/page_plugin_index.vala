public class Slate.Init.PagePluginIndex {
	
	public static Slate.Interface.Page.Service.PagePlugin.Index get_plugin_index(){
		var index = new Slate.Page.Service.PagePluginIndex();
		// Builtin Plugins
		info("Adding builtin plugin slate.redirect_dialog");
		index.add_plugin_loader(new Slate.Page.PluginLoader.RedirectDialog());
		info("Adding builtin plugin slate.autoredirect");
		index.add_plugin_loader(new Slate.Page.PluginLoader.Autoredirect());
		info("Adding builtin plugin slate.document_auto_parser");
		index.add_plugin_loader(new Slate.Page.PluginLoader.DocumentAutoParser());
		info("Adding builtin plugin slate.gemini_input_to_document");
		index.add_plugin_loader(new Slate.Page.PluginLoader.GeminiInputToDocument());
		
		return index;
	}
	
}

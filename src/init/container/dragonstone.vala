public class Slate.Init.Container.Dragonstone {
	
	public static Slate.Page.BackendIntegration.Container? get_smallnet_container(Slate.Interface.Page.Service.PagePlugin.Index page_plugin_index){
		var default_store_registry = new Slate.Registry.StoreRegistry.default_configuration();
		var gemini_store = new Slate.Store.Gemini();
		var gopher_store = new Slate.Store.Gopher();
		var finger_store = new Slate.Store.Finger();
		default_store_registry.add_resource_store("gemini://", gemini_store);
		default_store_registry.add_resource_store("gopher://", gopher_store);
		default_store_registry.add_resource_store("finger://", finger_store);
		string default_cachedir = GLib.Environment.get_user_cache_dir();
		var default_store = new Slate.Store.Switch(default_cachedir+"/slate", default_store_registry);
		var default_session = new Slate.Session.Default(default_store);
		var container = new Slate.Page.BackendIntegration.Container(default_session);
		container.page_plugin_index = page_plugin_index;
		container.default_page_plugins = {"slate.autoredirect", "slate.redirect_dialog", "slate.document_auto_parser","slate.gemini_input_to_document"};
		return container;
	}
	
	public static Slate.Page.BackendIntegration.Container? get_file_container(Slate.Interface.Page.Service.PagePlugin.Index page_plugin_index){
		var default_store_registry = new Slate.Registry.StoreRegistry.default_configuration();
		var file_store = new Slate.Store.File();
		default_store_registry.add_resource_store("file://", file_store);
		string default_cachedir = GLib.Environment.get_user_cache_dir();
		var default_store = new Slate.Store.Switch(default_cachedir+"/slate", default_store_registry);
		var default_session = new Slate.Session.Default(default_store);
		var container = new Slate.Page.BackendIntegration.Container(default_session);
		container.landing_page_address = "file://"+GLib.Environment.get_home_dir();
		container.page_plugin_index = page_plugin_index;
		container.default_page_plugins = {"slate.document_auto_parser"};
		return container;
	}
	
}

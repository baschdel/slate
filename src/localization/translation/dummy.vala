public class Slate.Localization.Translation.Dummy : Object, Slate.Interface.Localization.Translation {
	
	public string get_localized_string(string text_id){
		return @"?_$text_id";
	}
	
}

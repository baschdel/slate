public class Slate.Localization.Translation.Map : Object, Slate.Interface.Localization.Translation {
	
	private HashTable<string,string> translations = new HashTable<string,string>(str_hash, str_equal);
	
	public void set_translation(string text_id, string translation){
		translations.set(text_id, translation);
	}
	
	public string get_localized_string(string text_id){
		string? text = translations.get(text_id);
		if (text != null) {
			return text;
		} else {
			return @"?_$text_id";
		}
	}
	
}

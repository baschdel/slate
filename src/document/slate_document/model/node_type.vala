public enum Slate.Document.SlateDocument.Model.NodeType {
	SECTION,
	ITEM,
	PARAGRAPH;
	
	public string to_string(){
		switch(this) {
			case SECTION:
				return "section";
			case ITEM:
				return "item";
			case PARAGRAPH:
				return "paragraph";
			default:
				return "unknown";
		}
	}
}

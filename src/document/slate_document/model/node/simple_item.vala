public class Slate.Document.SlateDocument.Model.Node.SimpleItem : Slate.Document.SlateDocument.Model.Node.SimpleBase {
	
	public SimpleItem(string uri, string? label, string? category = null, string? description = null){
		this.uri = uri;
		this.text = label;
		this.category = category;
		this.description = description;
	}
	
	  /////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Node //
	/////////////////////////////////////////////////
	
	public override Slate.Document.SlateDocument.Model.NodeType get_node_type(){
		return ITEM;
	}
	
}

public class Slate.Document.SlateDocument.Model.Node.SimpleParagraph : Slate.Document.SlateDocument.Model.Node.SimpleBase {
	
	/*
		Simple Paragraphs just contain plain Text without any fancy formatting.
		They are based on the Slate.Document.SlateDocument.Model.Node.SimpleBase class
	*/
	
	// a class constructor that takes initial values, that are important for Simple Paragraphs
	public SimpleParagraph(string text, Slate.Document.SlateDocument.Model.WrapMode wrap_mode = WRAP, string? description = null, string? uri = null){
		this.text = text;
		this.wrap_mode = wrap_mode;
		this.description = description;
		this.uri = uri;
	}
	
	  /////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Node //
	/////////////////////////////////////////////////
	
	// implement the get_node_type() method
	// This is also part of the homework in case you wondered
	public override Slate.Document.SlateDocument.Model.NodeType get_node_type(){
		return PARAGRAPH;
	}
	
}

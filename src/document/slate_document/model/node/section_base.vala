public class Slate.Document.SlateDocument.Model.Node.SectionBase : Slate.Document.SlateDocument.Interface.Node, Slate.Document.SlateDocument.Model.Node.SimpleBase {
	
	protected List<Slate.Document.SlateDocument.Interface.Node> nodes = new List<Slate.Document.SlateDocument.Interface.Node>();
	
	public SectionBase(string? title, string? category = null, string? description = null, string? uri = null){
		this.text = title;
		this.category = category;
		this.description = description;
		this.uri = uri;
	}
	
	public void append_node(Slate.Document.SlateDocument.Interface.Node node) {
		nodes.append(node);
		this.node_modified(this);
	}
	
	public void prepend_node(Slate.Document.SlateDocument.Interface.Node node) {
		nodes.prepend(node);
		this.node_modified(this);
	}
	
	public void remove_node(Slate.Document.SlateDocument.Interface.Node node) {
		nodes.remove(node);
		this.node_modified(this);
	}
	
	  /////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Node //
	/////////////////////////////////////////////////
	
	public override Slate.Document.SlateDocument.Model.NodeType get_node_type(){
		return SECTION;
	}
	
	// Subnodes
	public override void foreach_subnode(Func<Slate.Document.SlateDocument.Interface.Node> cb){
		nodes.foreach(cb);
	}
	
	public override uint32 get_subnode_count(){
		return nodes.length();
	}
	
	public override Slate.Document.SlateDocument.Interface.Node? get_nth_subnode(uint32 n){
		return nodes.nth_data((uint) n);
	}
	
}

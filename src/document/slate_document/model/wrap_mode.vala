public enum Slate.Document.SlateDocument.Model.WrapMode {
	UNKNOWN,
	PREFORMATTED,
	WRAP;
	
	public string to_string(){
		switch(this) {
			case PREFORMATTED:
				return "preformatted";
			case WRAP:
				return "wrap";
			case UNKNOWN:
			default:
				return "unknown";
		}
	}
}

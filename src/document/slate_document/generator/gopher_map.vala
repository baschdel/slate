public class Slate.Document.SlateDocument.Generator.GopherMap : Slate.Document.SlateDocument.Interface.Generator.Gopher, Object {
	
	public Slate.Document.SlateDocument.Model.Node.SectionBase root_node;
	public Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes gophertypes;
	
	construct {
		root_node = new Slate.Document.SlateDocument.Model.Node.SectionBase(null, null, null, null);
	}
	
	public GopherMap(string? uri, Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes gophertypes){
		this.gophertypes = gophertypes;
		this.root_node.set_uri(uri);
	}
	
	  /////////////////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Generator.Gopher //
	/////////////////////////////////////////////////////////////
	
	public void append_gopher_token(unichar gophertype, string text, string selector, string host, string port){
		//TODO: group items when they share the same gophertype class
		var gophertype_class = gophertypes.get_gophertype_class(gophertype);
		Slate.Document.SlateDocument.Model.Node.SimpleBase? node = null;
		if (gophertype_class != null) {
			switch (gophertype_class) {
				case TEXT:
				case ERROR:
					node = new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(text, PREFORMATTED);
					if (gophertype_class == ERROR) {
						node.set_category("error");
					}
					break;
				case LINK:
				case SEARCH:
					string? uri = gophertypes.get_uri(gophertype, host, port, selector);
					if (uri != null) {
						if (text != "") {
							node = new Slate.Document.SlateDocument.Model.Node.SimpleItem(uri, text);
						} else {
							node = new Slate.Document.SlateDocument.Model.Node.SimpleItem(uri, null);
						}
						if (gophertype_class == SEARCH) {
							node.set_category("form");
						}
					}
					break;
				default:
					break;
			}
		}
		if (node != null && text == "") {
			node.set_category("empty");
		}
		if (node == null) {
			node = new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(@"$gophertype$text\t$selector\t$host\t$port", PREFORMATTED);
			node.set_category("error.document_error.unknown_gophertype");
		}
		root_node.append_node(node);
	}
	
	public Slate.Document.SlateDocument.Interface.Node get_root_node(){
		return root_node;
	}
	
}

public class Slate.Document.SlateDocument.Generator.DragonstoneFileListing : Slate.Document.SlateDocument.Interface.Generator.DragonstoneFileListing, Object {
	
	public Slate.Document.SlateDocument.Model.Node.SectionBase root_node;
	public Slate.Document.SlateDocument.Model.Node.SectionBase navigation_node;
	public Slate.Document.SlateDocument.Model.Node.SectionBase list_node;
	
	construct {
		root_node = new Slate.Document.SlateDocument.Model.Node.SectionBase(null, null, null, null);
		navigation_node = new Slate.Document.SlateDocument.Model.Node.SectionBase(null, "navigation", null, null);
		list_node = new Slate.Document.SlateDocument.Model.Node.SectionBase(null, "listing", null, null);
		root_node.append_node(navigation_node);
		root_node.append_node(list_node);
	}
	
	public DragonstoneFileListing(string source_uri) {
		root_node.set_uri(source_uri);
	}
	
	  /////////////////////////////////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Generator.DragonstoneFileListing //
	/////////////////////////////////////////////////////////////////////////////
	
	public void append_token(string type, string uri, string? name){
		string _name;
		if (name != null) {
			_name = name;
		} else {
			_name = uri;
		}
		if (type == "THIS") {
			root_node.set_text(_name);
			list_node.set_text(_name);
			return;
		} 
		bool is_list_item = false;
		string? category = null;
		switch (type){
			case "FILE":
				category = "file";
				is_list_item = true;
				break;
			case "DIRECTORY":
				category = "listing";
				is_list_item = true;
				break;
			case "HOME":
				category = "navigation.home";
				break;
			case "ROOT":
				category = "navigation.root";
				break;
			case "PARENT":
				category = "navigation.parent";
				break;
			default:
				break;
		}
		var item_node = new Slate.Document.SlateDocument.Model.Node.SimpleItem(uri, _name);
		if (is_list_item){
			list_node.append_node(item_node);
		} else {
			navigation_node.append_node(item_node);
		}
	}
	
	
	public Slate.Document.SlateDocument.Interface.Node get_root_node(){
		return root_node;
	}
	
	
	
}

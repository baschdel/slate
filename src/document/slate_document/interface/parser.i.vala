public interface Slate.Document.SlateDocument.Interface.Parser : Object {
	
	/*
		This interface specifies how a parser should look like after it was set up.
	*/
	
	public signal void on_root_node_updated(Slate.Document.SlateDocument.Interface.Node node);
	public signal void on_finished(Slate.Document.SlateDocument.Interface.Node node);
	
	public abstract async void process(Cancellable? cancellable) throws Error;
	
	public abstract Slate.Document.SlateDocument.Interface.Node? get_root_node();
	
	public abstract bool has_started();
	public abstract bool has_finished();
	
}

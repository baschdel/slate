public interface Slate.Document.SlateDocument.Interface.Node : Object {
	
	/*
		Slate Documents are made of objects implementing thins interface.
		Documents are Trees of nodes, so to refer to a document, you simply hand out its root node.
		Nodes have no reference to their parent nodes, wich makes restricting access to only  particular part of a document as easy as only handing out this subnode.
		Since this implementation with all its gobject dependencys (while easy to use) is not as efficent as it could be, you should not use it with large documents.
	*/
	
	// this signal should be fired when anything about this node changes
	// It contains a self reference, so that one handler function can be used for multiple nodes
	public signal void node_modified(Slate.Document.SlateDocument.Interface.Node node);
	
	/*
		Below are abstract definitions of funtions, that have to be implemented by every class that claims to support this interface.
		(Yes this comment is part of an assignment)
	*/
	public abstract Slate.Document.SlateDocument.Model.NodeType get_node_type(); //What kind of node do we have
	public abstract string get_node_id(); //uuid used to reidentify a node without having to keep a pointer
	public abstract string? get_node_index_id(); // The id that would be used in an uri-index
	public abstract string? get_node_category(); // The category of the node

	public abstract string? get_node_text(); // The node content/label/headline
	public abstract string? get_node_description(); // Accessibility label / summary
	public abstract string? get_node_uri(); // link destination uri / section source uri
	
	// Subnodes
	/*
		The functions below peovide default implementations and therefore do not have to be implemented.
		The default functionality can be overriden with the desired behaviour.
	*/
	// calls the cb once for each child node in oder from first to last.
	public virtual void foreach_subnode(Func<Slate.Document.SlateDocument.Interface.Node> cb){ ; }
	// returns how many children this node has
	public virtual uint32 get_subnode_count(){ return 0; }
	// returns the nth subnode nor null if the node does not exist. ids start at 0 and end at the first id returning null
	public virtual Slate.Document.SlateDocument.Interface.Node? get_nth_subnode(uint32 n){ return null; }
	
	// Text information
	// returns an enum that tells us wether the text is preformatted, may be wrapped or default settings apply
	public virtual Slate.Document.SlateDocument.Model.WrapMode get_preferred_text_wrap_mode(){ return UNKNOWN; }
	
	// Debug serializer
	// returns a text representation of the node and its children wich is useful for debugging
	public string debug_serialize_node(string prefix = ""){
		// initalize the output with the line prefix and the node type
		string output = @"$prefix$(this.get_node_type())";
		// if the node category is defined append it to the output
		var category = this.get_node_category();
		if (category != null) {
			output+=@".$category";
		}
		// if the node index_id is defined append it to the output
		var index_id = this.get_node_index_id();
		if (index_id != null) {
			output += @"#$index_id";
		}
		// if the node uri is defined append it to the output in a new line after the line prefix and an arrow
		var uri = this.get_node_uri();
		if (uri != null) {
			output += @"\n$prefix=> $uri";
		}
		// the same with a slightly different format for each line of the description
		var description = this.get_node_description();
		if (description != null) {
			foreach(string line in description.split("\n")){
				output += @"\n$prefix * $line";
			}
		}
		// output the wrap mode if it is known
		var wrap_mode = this.get_preferred_text_wrap_mode();
		if (wrap_mode != UNKNOWN) {
			output += @"\n$prefix TEXTMODE: $wrap_mode";
		}
		// output each line of text, prefixed with the usual foo
		var text = this.get_node_text();
		if (text != null) {
			foreach(string line in text.split("\n")){
				output += @"\n$prefix   $line";
			}
		}
		// iterate over all subnodes
		this.foreach_subnode((node) => {
			// append a little header to the output
			output += @"\n$prefix ┌── SUBNODE ─────";
			// recursively call this funtion on the cild node with a prefix matching the header
			output += "\n"+node.debug_serialize_node(prefix+" │");
		});
		// And return the output, quite literally
		return output;
	}
}

public interface Slate.Document.SlateDocument.Interface.Generator.DragonstoneFileListing : Object {
	
	public abstract void append_token(string type, string uri, string? name);
	public abstract Slate.Document.SlateDocument.Interface.Node get_root_node();
	
}

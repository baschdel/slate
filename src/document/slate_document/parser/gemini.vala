public class Slate.Document.SlateDocument.Parser.Gemini : Slate.Document.SlateDocument.Parser.Base {
	
	public Gemini(Slate.Interface.Page.Service.Resource.Resource resource){
		this.resource = resource;
	}
	
	  //////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Parser.Base //
	//////////////////////////////////////////////
	
	//returns null when finished can possibly hang because of the input stream
	public override async void parse(InputStream input_stream, string? source_uri, Cancellable? cancellable) throws Error {
		var data_stream = new DataInputStream(input_stream);
		var markup = new Slate.Document.SlateDocument.Generator.GeminiMarkup(null, source_uri);
		
		this.return_root_node(markup.get_root_node());
		
		bool preformatted_block = false;
		string? line = null;
		
		while ((line = yield data_stream.read_line_async(GLib.Priority.DEFAULT, cancellable)) != null) {
			bool is_text = true;
			// if line is valid utf-8
			if (line.validate(line.length)){
				// if we have a preformatted block toggle line
				if (line.has_prefix("```")){
					// toggle the preformatted_block flag
					preformatted_block = !preformatted_block;
					// if we have additional text on this line
					if (line.length > 3){
						// get this text without sourrounding whitspace caracters
						string alttext = line.substring(3).strip();
						// if this text wasn't just whitespaces
						if (alttext != "") {
							// if we just started a preformatted block
							if (preformatted_block) {
								// pass it on as the start of a preformatted block
								markup.append_gemini_token(PREFORMATTED_TEXT_START, alttext);
							} else {
								// else pass it on as the end of a preformatted block
								markup.append_gemini_token(PREFORMATTED_TEXT_END, alttext);
							}
						}
					}
					is_text=false; // this isn't a text line
				}
				// if we are not in a preformatted block
				if (!preformatted_block && is_text){
					// if a line starts with three hashes
					if (line.has_prefix("###")) {
						is_text = false; // this isn't a text line
						// pass on the rest of the line as content of a H3 token
						markup.append_gemini_token(H3, line.substring(3).strip());
					// if a line starts with two hashes
					} else if (line.has_prefix("##")) {
						is_text = false; // this isn't a text line
						// pass on the rest of the line as content of a H2 token
						markup.append_gemini_token(H2, line.substring(2).strip());
					// if a line starts with one hash
					} else if (line.has_prefix("#")) {
						is_text = false; // this isn't a text line
						// pass on the rest of the line as content of a H1 token
						markup.append_gemini_token(H1, line.substring(1).strip());
					// if the line starts with a star followed by a space
					} else if (line.has_prefix("* ")) {
						is_text = false; // this isn't a text line
						// pass on the rest of the line as content of a LIST_ITEM token
						markup.append_gemini_token(LIST_ITEM, line.substring(2).strip());
					// if the line starts with a greater than sign
					} else if (line.has_prefix(">")) {
						is_text = false; // this isn't a text line
						// pass on the rest of the line as content of a QUOTE token
						markup.append_gemini_token(QUOTE, line.substring(1).strip());
					// if the line starts with a link arrow
					} else if (line.has_prefix("=>")) {
						// define uri and description as empty
						var uri = "";
						string? description = null;
						// strip the rest of the line of sourrounding spaces
						var uri_and_text = line.substring(2).strip();
						// find out where the first space is
						var spaceindex = uri_and_text.index_of_char(' ');
						// find out where the first tab is
						var tabindex = uri_and_text.index_of_char('\t');
						// if there are no spaces and no tabs …
						if (spaceindex < 0 && tabindex < 0){
							// no description
							// uri everything that is left
							uri = uri_and_text;
						// if tabs come before spaces or no spaces are present
						} else if ((tabindex > 0 && tabindex < spaceindex) || spaceindex < 0){
							// tab seperated description
							// uri is everything until the first tab
							uri = uri_and_text.substring(0,tabindex);
							// description is everything after the first tab minus potatial sourrounding whitespaces
							description = uri_and_text.substring(tabindex).strip();
						} else if ((spaceindex > 0 && spaceindex < tabindex) || tabindex < 0){
							// space seperated description
							// uri is everything until the first space
							uri = uri_and_text.substring(0,spaceindex);
							// description is everything after the first space minus potatial sourrounding whitespaces
							description = uri_and_text.substring(spaceindex).strip();
						}
						is_text = false; // this isn't a text line
						// pass on the uri and potential descriton as a LINK token
						markup.append_gemini_token(LINK, uri, description);
					}
					
				}
				// if this is a text line without special meaning
				if (is_text){
					// if this is an empty line outside a preformatted block
					if (line == "" && !preformatted_block) {
						// pass on as an EMPTY_LINE token with empty content
						markup.append_gemini_token(EMPTY_LINE, "");
					// if not
					} else {
						// if this is inside a preformatted block
						if (preformatted_block) {
							// pass on as a PREFORMATTED_TEXT token
							markup.append_gemini_token(PREFORMATTED_TEXT, line);
						} else {
							// pass on as a normal TEXT token
							markup.append_gemini_token(TEXT, line);
						}
					}
				}
			}
		}
		markup.append_gemini_token(END_OF_DOCUMENT, "");
		data_stream.close();
	}
	
}

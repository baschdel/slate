public class Slate.Document.SlateDocument.Parser.DragonstoneFileListing : Slate.Document.SlateDocument.Parser.Base {
	
	public DragonstoneFileListing (Slate.Interface.Page.Service.Resource.Resource resource){
		this.resource = resource;
	}
	
	  //////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Parser.Base //
	//////////////////////////////////////////////
	
	//returns null when finished can possibly hang because of the input stream
	public override async void parse(InputStream input_stream, string? source_uri, Cancellable? cancellable) throws Error {
		var data_stream = new DataInputStream(input_stream);
		var listing = new Slate.Document.SlateDocument.Generator.DragonstoneFileListing(source_uri);
		
		this.return_root_node(listing.get_root_node());
		
		string? line = null;
		
		while ((line = yield data_stream.read_line_async(GLib.Priority.DEFAULT, cancellable)) != null) {
			// if line is valid utf-8
			if (line.validate(line.length)){
				string[] tokens = line.strip().split("\t",3);
				if (tokens.length == 2){
					listing.append_token(tokens[0], tokens[1], null);
				} else if (tokens.length == 3){
					listing.append_token(tokens[0], tokens[1], tokens[2]);
				}
			}
		}
		data_stream.close();
	}
	
}

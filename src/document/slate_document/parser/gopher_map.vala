public class Slate.Document.SlateDocument.Parser.GopherMap : Slate.Document.SlateDocument.Parser.Base {
	
	public Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes gophertypes;
	
	public GopherMap(Slate.Interface.Page.Service.Resource.Resource resource, Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes gophertypes){
		this.gophertypes = gophertypes;
		this.resource = resource;
	}
	
	  //////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Parser.Base //
	//////////////////////////////////////////////
	
	//returns null when finished can possibly hang because of the input stream
	public override async void parse(InputStream input_stream, string? source_uri, Cancellable? cancellable) throws Error {
		var data_stream = new DataInputStream(input_stream);
		var gophermap = new Slate.Document.SlateDocument.Generator.GopherMap(source_uri, gophertypes);
		
		this.return_root_node(gophermap.get_root_node());
		
		string? line = null;
		unichar lasttype = '\0';
		
		while ((line = yield data_stream.read_line_async(GLib.Priority.DEFAULT, cancellable)) != null) {
			if (line.validate(line.length)) {
				var tokens = line.split("\t");
				if(tokens.length >= 4){
					unichar gophertype = 'i';
					string text = "";
					if (tokens[0].length != 0){
						gophertype = tokens[0].get_char(0);
						text = tokens[0].substring(1);//human text
					}
					var selector = tokens[1].strip(); //look for url in here
					var host = tokens[2].strip();
					var port = tokens[3].strip();
					
					//Replace lasttype with gophertype
					if(gophertype == '+'){
						gophertype = lasttype;
					}
					lasttype = gophertype;
					//debug(@"Appending gopher token: $gophertype $text");
					gophermap.append_gopher_token(gophertype, text, selector, host, port);
				}
			}
		}
		
		data_stream.close();
	}
}

public class Slate.Util.Uri {
	
	private string? _uri = null;

	private string? _scheme = null;
	private string? _authority = null;
	private string? _path = null;
	private string? _query = null; // ..?<query>
	private string? _index = null; // ..#<index>
	
	//parsed authority
	private string? _username = null;
	private string? _hostinfo = null;
	
	//parse hostinfo
	private string? _hostname = null;
	private string? _port = null;
	
	// dirty flags for generating the components
	private bool uri_outdated = false;
	private bool authority_outdated = false;
	private bool hostinfo_outdated = false;
	
	//dirty flags for parsing the components
	private bool uri_unparsed = false;
	private bool authority_unparsed = false;
	private bool hostinfo_unparsed = false;
	
	public void reset(){
		_uri = null;

		_scheme = null;
		_authority = null;
		_path = null;
		_query = null;
		_index = null;
		
		_username = null;
		_hostinfo = null;
		
		_hostname = null;
		_port = null;
		
		uri_outdated = false;
		authority_outdated = false;
		hostinfo_outdated = false;
		
		uri_unparsed = false;
		authority_unparsed = false;
		hostinfo_unparsed = false;
	}
	
	public Uri(string uri){
		this.uri = uri;
	}
	
	public Uri.blank(){
		// do nothing
	}
	
	private void on_uri_component_set(){
		//debug("on_uri_component_set()");
		if (uri_unparsed) {
			parse_uri();
		}
		uri_outdated = true;
	}
	
	private void on_authority_component_set(){
		//debug("on_authority_component_set()");
		if (authority_unparsed) {
			parse_authority();
		}
		authority_outdated = true;
		uri_outdated = true;
	}
	
	private void on_hostinfo_component_set(){
		//debug("on_hostinfo_component_set()");
		if (hostinfo_unparsed) {
			parse_hostinfo();
		}
		hostinfo_outdated = true;
		authority_outdated = true;
		uri_outdated = true;
	}
	
	private void on_uri_component_get(){
		//debug("on_uri_component_get()");
		if (uri_unparsed) {
			parse_uri();
		}
	}
	
	private void on_authority_component_get(){
		//debug("on_authority_component_get()");
		if (authority_unparsed) {
			parse_authority();
		}
	}
	
	private void on_hostinfo_component_get(){
		//debug("on_hostinfo_component_get()");
		if (hostinfo_unparsed) {
			parse_hostinfo();
		}
	}
	
	private static string? escape_base(string? unescaped){
		if (unescaped == null) {
			return null;
		} else {
			return unescaped.replace(" ","%20").replace("\t","%09").replace("\"","%22").replace("'","%27");
		}
	}
	
	// public values
	
	public string? uri {
		get {
			if (uri_outdated) {
				assemble_uri();
			}
			return _uri;
		}
		set {
			uri_unparsed = true;
			authority_unparsed = true;
			hostinfo_unparsed = true;
			uri_outdated = false;
			authority_outdated = false;
			hostinfo_outdated = false;
			this._uri = escape_base(value);
		}
	}
	
	// parsed uri
	public string? scheme {
		get {
			on_uri_component_get();
			return _scheme;
		}
		set {
			//debug("set scheme");
			on_uri_component_set();
			this._scheme = escape_base(value).replace("/","%2f").replace(":","%3a");
		}
	}
	
	public string? authority {
		get {
			on_uri_component_get();
			if (authority_outdated) {
				assemble_authority();
			}
			return _authority;
		}
		set {
			//debug("set authority");
			on_uri_component_set();
			this.authority_unparsed = true;
			this.hostinfo_unparsed = true;
			this.authority_outdated = false;
			this.hostinfo_outdated = false;
			this._authority = escape_base(value).replace("/","%2f");
		}
	}
	
	public string? path {
		get {
			if (uri_unparsed) {
				parse_uri();
			}
			return _path;
		}
		set {
			//debug("set path");
			on_uri_component_set();
			this._path = escape_base(value).replace("?","%3f").replace("#","%23");
		}
	}
	
	public string? query {
		get {
			if (uri_unparsed) {
				parse_uri();
			}
			return _query;
		}
		set {
			//debug("set query");
			on_uri_component_set();
			this._query = escape_base(value).replace("#","%23");
		}
	}
	
	public string? index {
		get {
			if (uri_unparsed) {
				parse_uri();
			}
			return _index;
		}
		set {
			//debug("set index");
			on_uri_component_set();
			this._index = escape_base(value);
		}
	}
	
	// parsed authority
	public string? username {
		get {
			on_authority_component_get();
			return _username;
		}
		set {
			on_authority_component_set();
			this._username = escape_base(value).replace("/","%2f").replace("@","%40");
		}
	}
	
	public string? hostinfo {
		get {
			on_authority_component_get();
			if (hostinfo_outdated) {
				assemble_hostinfo();
			}
			return _hostinfo;
		}
		set {
			on_authority_component_set();
			this.hostinfo_unparsed = true;
			this.hostinfo_outdated = false;
			this._hostinfo = escape_base(value).replace("/","%2f");
		}
	}
	
	//parsed hostinfo
	public string? hostname {
		get {
			on_hostinfo_component_get();
			return _hostname;
		}
		set {
			on_hostinfo_component_set();
			this._hostname = escape_base(value).replace("/","%2f").replace(":","%3a").replace("[","%5b").replace("]","%5d");
		}
	}
	
	public string? port {
		get {
			on_hostinfo_component_get();
			return _port;
		}
		set {
			on_hostinfo_component_set();
			this._port = escape_base(value).replace("/","%2f");
		}
	}
	
	private void assemble_uri(){
		//debug("assemble_uri()");
		bool any_component_present = false;
		string final_uri = "";
		if (scheme != null){
			any_component_present = true;
			final_uri = @"$scheme:";
		}
		if (authority != null){
			any_component_present = true;
			final_uri = @"$final_uri//$authority";
		}
		if (path != null){
			any_component_present = true;
			if (path.has_prefix("//") && authority == null) {
				//prefix an empty authority to prevent the path from looking like the authority
				//only applies if there isn't already an authority present
				final_uri = @"$final_uri//$path";
			} else if ((!path.has_prefix("/")) && authority != null){
				//prefix the path with a / when an authority is present
				//this will turn it from a relative path into an absolute path, but at least it will stay a path
				final_uri = @"$final_uri/$path";
			} else {
				final_uri = @"$final_uri$path";
			}
		}
		if (query != null){
			any_component_present = true;
			final_uri = @"$final_uri?$query";
		}
		if (index != null){
			any_component_present = true;
			final_uri = @"$final_uri#$index";
		}
		if (any_component_present) {
			this._uri = final_uri;
		} else {
			this._uri = null;
		}
	}
	
	private void assemble_authority(){
		//debug("assemble_authority()");
		bool any_component_present = false;
		string final_authority = "";
		if (username != null) {
			any_component_present = true;
			final_authority = @"$username@";
		}
		if (hostinfo != null) {
			any_component_present = true;
			final_authority=@"$final_authority$hostinfo";
		}
		if (any_component_present) {
			this.authority = final_authority;
		} else {
			this.authority = null;
		}
	}
	
	private void assemble_hostinfo(){
		//debug("assemble_hostinfo()");
		bool any_component_present = false;
		string final_hostinfo = "";
		if (hostname != null) {
			any_component_present = true;
			if (hostname.contains(":")) {
				final_hostinfo = @"[$hostname]";
			} else {
				final_hostinfo = hostname;
			}
		}
		if (port != null) {
			any_component_present = true;
			final_hostinfo=@"$final_hostinfo:$port";
		}
		if (any_component_present) {
			this.hostinfo = final_hostinfo;
		} else {
			this.hostinfo = null;
		}
	}
	
	private void parse_uri(){
		//debug(@"parse_uri()");
		if (uri != null) {
			// extract index
			int index_of_hash = uri.index_of_char('#');
			if (index_of_hash >= 0) {
				if (uri.length > index_of_hash+1) {
					this._index = uri.substring(index_of_hash+1);
				} else {
					this._index = "";
				}
				//print(@"index: '$_index'\n");
			} else {
				this._index = null;
				index_of_hash = _uri.length;
			}
			//extract query
			int index_of_questionmark = uri.index_of_char('?');
			if (index_of_questionmark >= 0 && index_of_questionmark < index_of_hash) {
				if (uri.length > index_of_questionmark+1) {
					this._query = uri.substring(index_of_questionmark+1, index_of_hash-index_of_questionmark-1);
				} else {
					this._query = "";
				}
				//print(@"index: '$_index'\n");
			} else {
				this._query = null;
				index_of_questionmark = index_of_hash;
			}
			// extract scheme
			int index_of_colon = uri.index_of_char(':');
			int index_of_slash = uri.index_of_char('/');
			if (index_of_colon > 0 && index_of_colon < index_of_questionmark && (index_of_colon < index_of_slash || index_of_slash < 0)) {
				if (uri.length > 0) {
					this._scheme = uri.substring(0, index_of_colon);
				} else {
					this._scheme = "";
				}
			}	else {
				this._scheme = null;
				index_of_colon = -1;
			}
			// extract path and authority
			int path_start = index_of_colon+1;
			if (index_of_slash == index_of_colon+1 && uri.length > index_of_slash) {
				int index_of_second_slash = uri.index_of_char('/', index_of_slash+1);
				if (index_of_second_slash == index_of_slash+1) {
					if (uri.length > index_of_second_slash) {
						path_start = uri.index_of_char('/', index_of_second_slash+1);
						if (path_start < 0) {
							this._authority = uri.substring(index_of_second_slash+1, index_of_questionmark-index_of_second_slash-1);
						} else {
							this._authority = uri.substring(index_of_second_slash+1, path_start-index_of_second_slash-1);
						}
					} else {
						path_start = -1;
						this._authority = "";
					}
				}
			}
			if (path_start >= 0){
				this._path = uri.substring(path_start, index_of_questionmark-path_start);
				if (path_start == index_of_colon+1) {
					this._authority = null;
				}
			} else {
				this._path = null;
			}
		} else {
			this._scheme = null;
			this._authority = null;
			this._path = null;
			this._query = null;
			this._index = null;
		}
		authority_outdated = false;
		authority_unparsed = true;
		uri_unparsed = false;
	}
	
	private void parse_authority(){
		//debug("parse_authority()");
		if (authority != null) {
			int index_of_at = authority.index_of_char('@');
			if (index_of_at > 0) {
				this._hostinfo = authority.substring(0,index_of_at);
				this._username = authority.substring(index_of_at);
			} else {
				this._hostinfo = authority;
				this._username = null;
			}
		} else {
			this._hostinfo = null;
			this._username = null;
		}
		hostinfo_outdated = false;
		hostinfo_unparsed = true;
		authority_unparsed = false;
	}
	
	private void parse_hostinfo(){
		//debug("parse_hostinfo()");
		if (hostinfo != null) {
			int start_of_hostname = 0;
			int length_of_hostname = -1;
			if (hostinfo.has_prefix("[")) {
				start_of_hostname = 1;
				length_of_hostname = hostinfo.index_of_char(']')-1;
			}
			int index_of_colon = hostinfo.index_of_char(':');
			if (index_of_colon > 0) {
				if (index_of_colon+1 < hostinfo.length) {
					this._port = hostinfo.substring(index_of_colon+1);
				} else {
					this._port = "";
				}
				if (length_of_hostname < 0) {
					length_of_hostname = index_of_colon;
				}
			} else {
				this._port = null;
			}
			this._hostname = hostinfo.substring(start_of_hostname, length_of_hostname);
		} else {
			this._hostname = null;
			this._port = null;
		}
		hostinfo_unparsed = false;
	}
	
	public uint16? get_port_number(){
		if (this.port != null){
			uint64 result;
			if (Slate.Util.Intparser.try_parse_unsigned(this.port,out result)) {
				if (result >= 0 && result <= uint16.MAX){
					return (uint16) result;
				}
			}
		}
		return null;
	}
	
	public string? get_filename(){
		if (path != null) {
			var tokens = path.split("/");
			return tokens[tokens.length-1];
		} else {
			return null;
		}
	}
	
	public bool is_absolute(){
		return scheme != null;
	}
	
	public static Uri join(Uri base_uri, Uri relative_uri){
		// if the relative uri isn't relative at all
		if (relative_uri.is_absolute()){
			return new Uri(relative_uri.uri);
		}
		//analyze relative uri
		bool relative_specifies_authority = relative_uri.authority != null;
		bool relative_has_absolute_path = false;
		bool relative_specifies_path = relative_uri.path != null;
		bool relative_specifies_query = relative_uri.query != null;
		bool relative_specifies_index = relative_uri.index != null;
		if (relative_specifies_path) {
			relative_has_absolute_path = relative_uri.path.has_prefix("/");
		}
		//collect parts
		var new_uri = new Uri.blank();
		new_uri.scheme = base_uri.scheme;
		if (relative_specifies_authority) {
			new_uri.authority = relative_uri.authority;
		} else {
			new_uri.authority = base_uri.authority;
		}
		if (relative_specifies_path) {
			if (relative_has_absolute_path || base_uri.path == null) {
				new_uri.path = join_paths("",relative_uri.path);
			} else {
				new_uri.path = join_paths(base_uri.path, relative_uri.path);
			}
		} else if (!relative_specifies_authority){
			new_uri.path = base_uri.path;
		}
		if (relative_specifies_query) {
			new_uri.query = relative_uri.query;
		} else if ((!relative_specifies_path) && (!relative_specifies_authority)) {
			new_uri.query = base_uri.query;
		}
		if (relative_specifies_index) {
			new_uri.index = relative_uri.index;
		} else if ((!relative_specifies_query) &&(!relative_specifies_path) && (!relative_specifies_authority)) {
			new_uri.index = base_uri.index;
		}
		return new_uri;
	}
	
	public static string join_paths(string basepath,string relativepath){
		Slate.Util.Stack<string> pathtokens = new Slate.Util.Stack<string>();
		if (basepath != ""){
			foreach(string token in basepath.split("/")){
				if (token == "."){ //ignore
				} else if (token == ".."){
					pathtokens.pop();
				} else {
					pathtokens.push(token); //let's assume that this does not contain ".."
				}
			}
			pathtokens.pop();
		}
		if (relativepath != ""){
			foreach(string token in relativepath.split("/")){
				if (token == "."){ //ignore
				} else if (token == ".."){
					pathtokens.pop();
				} else {
					pathtokens.push(token); //let's assume that this does not contain ".."
				}
			}
		}
		string new_path = "";
		bool first = true;
		foreach(string token in pathtokens.list){
			if (first){
				first = false;
				new_path = token;
			} else {
				new_path = new_path+"/"+token;
			} 
		}
		if (relativepath.has_suffix("/") && !new_path.has_suffix("/")){
			new_path = new_path+"/";
		}
		return new_path;
	}
	
	public string? get_combined_path_query(){
		if (path != null) {
			if (query != null) {
				return @"$path?$query";
			} else {
				return path;
			}
		} else {
			if (query != null) {
				return @"?$query";
			} else {
				return null;
			}
		}
	}
	
	public string? get_combined_path_query_index(){
		string path_query = this.get_combined_path_query();
		if (path_query != null) {
			if (index != null) {
				return @"$path_query#$index";
			} else {
				return path_query;
			}
		} else {
			if (index != null) {
				return @"#$index";
			} else {
				return null;
			}
		}
	}
	
	public static string strip_querys(string uri){
		unichar[] pathends = {'?','#','\t'};
		var pathend = uri.length;
		foreach(unichar end in pathends){
			var index = uri.index_of_char(end);
			if (index < pathend && index >= 0){pathend = index;}
		}
		return uri[0:pathend];
	}
	
	public static Uri? maybe(string? uri){
		if (uri != null) {
			return new Uri(uri);
		} else {
			return null;
		}
	}
}

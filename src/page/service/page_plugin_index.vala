public class Slate.Page.Service.PagePluginIndex : Slate.Interface.Page.Service.PagePlugin.Index, Object {
	
	private HashTable<string,Slate.Interface.Page.Service.PagePlugin.Loader> plugin_loaders = new HashTable<string,Slate.Interface.Page.Service.PagePlugin.Loader>(str_hash, str_equal);
	
	public void add_plugin_loader(Slate.Interface.Page.Service.PagePlugin.Loader loader){
		if (loader.get_plugin_id() == null) {
			critical("Plugin id is null! Make sure the plugin id is a const, not just a static when using the generate_plugin_loader.lua script! (Not adding plugin)");
			critical("(If you are not a developer please report the above message as a bug!)");
			return;
		}
		plugin_loaders.set(loader.get_plugin_id(), loader);
	}
	
	  ///////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.PagePlugin.Index //
	///////////////////////////////////////////////////
	
	public Slate.Interface.Page.Service.PagePlugin.Loader? get_plugin_loader(string id){
		return plugin_loaders.get(id);
	}
	
	public void foreach_plugin_loader(Func<Slate.Interface.Page.Service.PagePlugin.Loader> cb){
		plugin_loaders.foreach((_,plugin_loader) => {
			cb(plugin_loader);
		});
	}
	
}

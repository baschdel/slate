public class Slate.Page.Service.Dialog : Slate.Interface.Page.Service.Dialog, Object {
	
	private HashTable<string,Slate.Interface.Ui.Dialog> dialogs = new HashTable<string,Slate.Interface.Ui.Dialog>(str_hash, str_equal);
	private HashTable<string,string> spawning_tasks = new HashTable<string,string>(str_hash, str_equal);
	
	  /////////////////////////////////////////
	 // Slate.Interface.Page.Service.Dialog //
	/////////////////////////////////////////
	
	public string add_dialog(Slate.Interface.Ui.Dialog dialog, string? spawning_task_id){
		lock (dialogs) {
			string dialog_id = GLib.Uuid.string_random();
			dialogs.set(dialog_id, dialog);
			if (spawning_task_id != null) {
				spawning_tasks.set(dialog_id, spawning_task_id);
			}
			dialog_added(dialog_id, dialog);
			return dialog_id;
		}
	}
	
	public bool remove_dialog(string dialog_id){
		lock (dialogs) {
			if (dialogs.remove(dialog_id)) {
				spawning_tasks.remove(dialog_id);
				dialog_removed(dialog_id);
				return true;
			} else {
				return false;
			}
		}
	}
	
	public void foreach_dialog(HFunc<string,Slate.Interface.Ui.Dialog> cb){
		dialogs.foreach(cb);
	}
	
	public Slate.Interface.Ui.Dialog? get_dialog(string dialog_id){
		return dialogs.get(dialog_id);
	}
	
	public string? get_spawning_task(string dialog_id){
		return spawning_tasks.get(dialog_id);
	}
	
}

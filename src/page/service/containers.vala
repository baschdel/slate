public class Slate.Page.Service.Containers : Object, Slate.Interface.Page.Service.Containers {
	
	private HashTable<string,Slate.Interface.Page.Container> containers = new HashTable<string,Slate.Interface.Page.Container>(str_hash, str_equal);
	
	  ///////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Containers //
	///////////////////////////////////////////////////
	
	public void set_container(string container_id, Slate.Interface.Page.Container? container){
		if (container != null){
			containers.set(container_id, container);
		} else {
			containers.remove(container_id);
		}
	}
	
	public Slate.Interface.Page.Container? get_contatiner(string container_id){
		return containers.get(container_id);
	}
	
	public bool has_container(string container_id){
		return containers.contains(container_id);
	}
	// will iterate over all containers, if an uri is specified, only over those who claim to be able to crate a tab for the uri.
	public void foreach_container(Func<string> cb, string? uri = null){
		containers.foreach((id, container) => {
			if (uri == null) {
				cb(id);
			} else if (container.can_construct_tab_for(uri)){
				cb(id);
			}
		});
	}
	
}

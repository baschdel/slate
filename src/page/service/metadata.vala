public class Slate.Page.Service.Metadata : Object, Slate.Interface.Page.Service.Metadata, Slate.Interface.Page.Service.Log.Logged {
	
	private HashTable<string,string> metadata = new HashTable<string,string>(str_hash, str_equal);
	
	  /////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Metadata //
	/////////////////////////////////////////////////
	
	public void set_page_metadata(string key, string? val, string module_name){
		if (val == null) {
			metadata.remove(key);
		} else {
			metadata.set(key, val);
		}
		on_page_metadata_change(key, val);
		if (val == null) {
			entry_submitted(module_name, "service.metadata", "remove", {key}, "", "access");
		} else {
			entry_submitted(module_name, "service.metadata", "set", {key,val}, "", "access");
		}
	}
	
	public string? get_page_metadata(string key, string module_name){
		return metadata.get(key);
	}
	
	public void foreach_page_metadata_key(Func<string> cb, string module_name){
		metadata.foreach((k,_) => {
			cb(k);
		});
	}
	
}

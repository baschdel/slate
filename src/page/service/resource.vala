public class Slate.Page.Service.Resource : Object, Slate.Interface.Page.Service.Resource.Service {
	
	private HashTable<string,Slate.Interface.Page.Service.Resource.Resource> resources = new HashTable<string,Slate.Interface.Page.Service.Resource.Resource>(str_hash, str_equal);
	
	  ///////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Resource.Service //
	///////////////////////////////////////////////////
	
	public bool is_resource_avaible(string resource_name){
		var resource = get_resource(resource_name);
		if (resource == null){
			return false;
		} else {
			if (resource.is_available()) {
				return true;
			} else {
				remove_resource(resource_name);
				return false;
			}
		}
	}
	
	public void foreach_resource(HFunc<string,Slate.Interface.Page.Service.Resource.Resource> cb){
		resources.foreach((resource_name, resource) => {
			if (resource.is_available()) {
				cb(resource_name, resource);
			} else {
				remove_resource(resource_name);
			}
		});
	}
	
	public void remove_resource(string resource_name){
		if (resources.remove(resource_name)) {
			this.resource_updated(resource_name, null);
		}
	}
	
	public void set_resource(string resource_name, Slate.Interface.Page.Service.Resource.Resource resource){
		resources.set(resource_name, resource);
		this.resource_updated(resource_name, resource);
	}
	
	public Slate.Interface.Page.Service.Resource.Resource? get_resource(string resource_name){
		return resources.get(resource_name);
	}
	
}

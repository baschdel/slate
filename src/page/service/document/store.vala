public class Slate.Page.Service.Document.Store : Slate.Interface.Page.Service.Document.Store, Object {
	
	private HashTable<string,Slate.Document.SlateDocument.Interface.Node> documents = new HashTable<string,Slate.Document.SlateDocument.Interface.Node>(str_hash, str_equal);
	
	  /////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Document.Store //
	/////////////////////////////////////////////////
	
	public void store_document(string name, Slate.Document.SlateDocument.Interface.Node node){
		documents.set(name, node);
		this.document_modified(name, node);
	}
	
	public void remove_document(string name){
		documents.remove(name);
		this.document_modified(name, null);
	}
	
	public void foreach_document(HFunc<string,Slate.Document.SlateDocument.Interface.Node> cb){
		documents.foreach(cb);
	}
	
	public Slate.Document.SlateDocument.Interface.Node? retrieve_document(string name){
		return documents.get(name);
	}
	
}

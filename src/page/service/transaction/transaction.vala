public class Slate.Page.Service.Transaction.Transaction : Object {
	
	/*
		This class is used internally by Slate.Page.Service.Transaction.Log
	*/
	
	public List<string> resources;
	public List<Slate.Interface.Page.Service.Transaction.LogEntry> log_entrys;
	public string uri;
	public string verb;
	public bool ended = false;
	public Slate.Interface.Page.Service.Transaction.LogEntry? latest_state = null;
	public Slate.Interface.Page.Service.Tasks.Task? task = null;
	
	public Transaction(string uri, string verb, Slate.Interface.Page.Service.Tasks.Task? task){
		this.uri = uri;
		this.verb = verb;
		this.task = task;
		this.log_entrys = new List<Slate.Interface.Page.Service.Transaction.LogEntry>();
		this.resources = new List<string>();
	}
	
	public bool append_log_entry(Slate.Interface.Page.Service.Transaction.LogEntry entry){
		lock(log_entrys){
			if (this.ended) {
				return false;
			} else {
				if (entry.topic == "state") {
					this.latest_state = entry;
				}
				this.log_entrys.append(entry);
				return true;
			}
		}
	}
	
	public bool end_transaction(){
		lock(log_entrys){
			if (this.ended) {
				return false;
			} else {
				this.ended = true;
				return true;
			}
		}
	}
	
	public bool append_resource(string resource_id){
		lock(log_entrys){
			if (this.ended) {
				return false;
			} else {
				this.resources.append(resource_id);
				return true;
			}
		}
	}
	
}

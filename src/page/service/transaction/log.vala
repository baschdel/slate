public class Slate.Page.Service.Transaction.Log : Object, Slate.Interface.Page.Service.Transaction.Log {
	
	private HashTable<string,Slate.Page.Service.Transaction.Transaction> transactions;
	private HashTable<string,Slate.Interface.Page.Service.Transaction.Resource> resources;
	private Slate.Interface.Page.Service.Tasks.Service task_service;
	
	public Log(Slate.Interface.Page.Service.Tasks.Service task_service){
		this.task_service = task_service;
		this.transactions = new HashTable<string,Slate.Page.Service.Transaction.Transaction>(str_hash, str_equal);
		this.resources = new HashTable<string,Slate.Interface.Page.Service.Transaction.Resource>(str_hash, str_equal);
	}
	
	  //////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Transaction.Log //
	//////////////////////////////////////////////////
	
	// reading
	
	public bool has_transaction(string transaction_id){
		return transactions.contains(transaction_id);
	}
	
	public string? get_transaction_uri(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			return transaction.uri;
		}
		return null;
	}
	
	public string? get_transaction_verb(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			return transaction.verb;
		}
		return null;
	}
	
	public Slate.Interface.Page.Service.Tasks.Task? get_transaction_task(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			return transaction.task;
		}
		return null;
	}
	
	public Slate.Interface.Page.Service.Transaction.LogEntry? get_transaction_state(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			return transaction.latest_state;
		}
		return null;
	}
	
	public bool? has_transaction_ended(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			return transaction.ended;
		}
		return null;
	}
	
	public void foreach_transaction_log_entry(string transaction_id, Func<Slate.Interface.Page.Service.Transaction.LogEntry> cb){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			transaction.log_entrys.foreach(cb);
		}
	}
	
	public void foreach_transaction(Func<string> cb){
		transactions.foreach((id,_) => {
			cb(id);
		});
	}
	
	public void foreach_resource(string transaction_id, Func<string> cb){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			transaction.resources.foreach(cb);
		}
	}
	
	public bool has_resource(string resource_id){
		return resources.contains(resource_id);
	}
	
	public Slate.Interface.Page.Service.Transaction.Resource? get_resource(string resource_id){
		return resources.get(resource_id);
	}
	
	// writing
	public string start_transaction(string uri, string verb = "download", Slate.Interface.Page.Service.Tasks.Task? task = null){
		string transaction_id = GLib.Uuid.string_random();
		lock (transactions) {
			while(this.has_transaction(transaction_id)){
				transaction_id = GLib.Uuid.string_random();
			}
		}
		transactions.set(transaction_id, new Slate.Page.Service.Transaction.Transaction(uri, verb, task));
		this.transaction_started(transaction_id, uri, verb, task);
		if (task != null) {
			task_service.add_task_object(task);
		}
		return transaction_id;
	}
	
	public bool end_transaction(string transaction_id){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			if (transaction.end_transaction()){
				this.transaction_ended(transaction_id);
				return true;
			}
		}
		return false;
	}
	
	public bool submit_log_entry(string transaction_id, Slate.Interface.Page.Service.Transaction.LogEntry entry){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			if (transaction.append_log_entry(entry)){
				this.log_entry_submitted(transaction_id, entry);
				return true;
			}
		}
		return false;
	}
	
	public string? submit_resource(string transaction_id, Slate.Interface.Page.Service.Resource.Resource? resource, string uri, uint64? final_size){
		var transaction = transactions.get(transaction_id);
		if (transaction != null) {
			if (!transaction.ended) {
				string resource_id = GLib.Uuid.string_random();
				lock (resources) {
					while(this.has_resource(resource_id)){
						resource_id = GLib.Uuid.string_random();
					}
				}
				if (transaction.append_resource(resource_id)) {
					var transaction_resource = new Slate.Interface.Page.Service.Transaction.Resource(resource, transaction_id, uri, final_size);
					resources.set(resource_id, transaction_resource);
					if (resource != null) {
						this.resource_added(resource_id, transaction_resource);
					} else {
						this.resource_updated(resource_id, transaction_resource);
					}
					return resource_id;
				}
			}
		}
		return null;
	}
	
	public bool add_resource(string resource_id, Slate.Interface.Page.Service.Resource.Resource resource){
		var transaction_resource = resources.get(resource_id);
		if (transaction_resource != null) {
			if (transaction_resource.add_resource(resource)){
				this.resource_added(resource_id, transaction_resource);
				return true;
			}
		}
		return false;
	}
	
	public bool update_resource_size(string resource_id, uint64 size, uint64? final_size = null){
		var resource = resources.get(resource_id);
		if (resource != null) {
			if (resource.update_size(size)){
				this.resource_updated(resource_id, resource);
				return true;
			}
		}
		return false;
	}
	
	public bool finish_resource(string resource_id, bool is_complete){
		var resource = resources.get(resource_id);
		if (resource != null) {
			if (resource.finish(is_complete)){
				this.resource_finished(resource_id, resource);
				return true;
			}
		}
		return false;
	}
}

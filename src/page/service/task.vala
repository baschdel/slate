public class Slate.Page.Service.Task : Slate.Interface.Page.Service.Tasks.Service, Object {
	
	private HashTable<string,Slate.Interface.Page.Service.Tasks.Task> tasks = new HashTable<string,Slate.Interface.Page.Service.Tasks.Task>(str_hash, str_equal);
	
	uint64 idcounter = 0;
	
	  ////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Service //
	////////////////////////////////////////////////
	
	public string add_task_object(Slate.Interface.Page.Service.Tasks.Task task){
		string task_id;
		lock(idcounter){
			task_id = @"#$idcounter";
			idcounter++;
		}
		tasks.set(task_id, task);
		this.task_added(task_id, task);
		return task_id;
	}

	public Slate.Interface.Page.Service.Tasks.Task? get_task_object(string task_id){
		return tasks.get(task_id);
	}
	
	public void foreach_registred_task(HFunc<string,Slate.Interface.Page.Service.Tasks.Task> cb, bool only_ongoing = false){
		if (!only_ongoing) {
			tasks.foreach(cb);
		} else {
			tasks.foreach((id, task) => {
				if (!task.has_task_finished()) {
					cb(id, task);
				}
			});
		}
	}
}

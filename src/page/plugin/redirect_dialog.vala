public class Slate.Page.Plugin.RedirectDialog : Slate.Interface.Page.Service.Tasks.Task, Object {
	
	public const string PAGE_PLUGIN_ID = "slate.redirect_dialog";
	public const string VERSION = "builtin";
	
	private string page_uri = "";
	private Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	private Slate.Interface.Page.Service.Tasks.Service task_service;
	private Slate.Interface.Page.Service.Dialog dialog_service;
	private Slate.Interface.Page.Service.Transaction.Log transaction_log;
	private string state = "initalizing";
	private string? dialog_id = null;
	private string? task_id = null;
	
	public RedirectDialog(Slate.Interface.Page.Service.Tasks.Service task_service, Slate.Interface.Page.Service.InternalNavigation internal_navigation, Slate.Interface.Page.Service.Transaction.Log transaction_log, Slate.Interface.Page.Service.Dialog dialog_service){
		this.dialog_service = dialog_service;
		this.transaction_log = transaction_log;
		this.internal_navigation = internal_navigation;
		this.task_service = task_service;
		// hook signals
		internal_navigation.current_uri_changed.connect(on_current_uri_changed);
		on_current_uri_changed(internal_navigation.get_current_uri());
		transaction_log.transaction_ended.connect(on_transaction_ended);
		state = "idle";
		task_id = task_service.add_task_object(this);
	}
	
	~RedirectDialog(){
		internal_navigation.current_uri_changed.disconnect(on_current_uri_changed);
		transaction_log.transaction_ended.disconnect(on_transaction_ended);
	}
	
	private void on_transaction_ended(string transaction_id){
		if (transaction_log.get_transaction_uri(transaction_id) == this.page_uri) {
			remove_dialog();
			var transaction_state = transaction_log.get_transaction_state(transaction_id);
			if (transaction_state.key == "redirect") {
				string[] redirect_uris = {};
				transaction_log.foreach_transaction_log_entry(transaction_id, (entry) => {
					if (entry.topic == "redirect" && entry.uri != null) {
						redirect_uris += entry.uri;
					}
				});
				if (redirect_uris.length > 0) {
					show_dialog(redirect_uris);
				}
			}
		}
	}
	
	private void remove_dialog(){
		if (dialog_id != null && dialog_service != null) {
			dialog_service.remove_dialog(dialog_id);
		}
		state = "idle";
		this.on_task_state_updated(this);
		this.on_actions_updated(this);
	}
	
	private void show_dialog(string[] uris){
		if (dialog_service != null) {
			lock (dialog_service) {
				remove_dialog();
				Slate.Interface.Ui.DialogAction[] actions = {};
				foreach (string uri in uris) {
					actions += new Slate.Ui.Dialog.Action.Redirect(uri, internal_navigation, "redirect_dialog");
				}
				var dialog = new Slate.Ui.Dialog.Dialog("redirect", null, uris.length == 1, actions);
				this.dialog_id = dialog_service.add_dialog(dialog, task_id);
				state = "showing_dialog";
				this.on_task_state_updated(this);
				this.on_actions_updated(this);
			}
		}
	}
	
	private void on_current_uri_changed(string uri){
		if (this.page_uri != uri) {
			remove_dialog();
			this.page_uri = uri;
		}
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Task //
	/////////////////////////////////////////////
	
	public string get_task_name(){
		return PAGE_PLUGIN_ID;
	}
	
	public string get_task_category(){
		return "ui";
	}
	
	public double? get_task_progress(){
		return null;
	}
	
	public string get_task_state(){
		return this.state;
	}
	
	public bool has_task_finished(){
		return state == "initalizing";
	}
	
	public string[] get_available_task_actions(){
		if (state == "showing_dialog"){
			return {"dismiss"};
		}
		return {};
	}
	
	public bool activate_task_action(string action){
		if (state == "showing_dialog" && action == "dismiss"){
			remove_dialog();
			return false;
		}
		return false;
	}
	
	public void foreach_task_property(Func<string> cb){
		cb("page_uri");
	}
	
	public string? read_task_property(string key){
		switch(key) {
			case "page_uri":
				return page_uri;
			default:
				return null;
		}
	}
	
}

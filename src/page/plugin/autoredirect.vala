public class Slate.Page.Plugin.Autoredirect : Slate.Interface.Page.Service.Tasks.Task, Object {
	
	public const string PAGE_PLUGIN_ID = "slate.autoredirect";
	public const string VERSION = "builtin";
	
	private string page_uri = "";
	private Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	private Slate.Interface.Page.Service.Tasks.Service task_service;
	private Slate.Interface.Page.Service.Transaction.Log transaction_log;
	private string state = "initalizing";
	private string? task_id = null;
	
	public Autoredirect(Slate.Interface.Page.Service.Tasks.Service task_service, Slate.Interface.Page.Service.InternalNavigation internal_navigation, Slate.Interface.Page.Service.Transaction.Log transaction_log){
		this.transaction_log = transaction_log;
		this.internal_navigation = internal_navigation;
		this.task_service = task_service;
		// hook signals
		internal_navigation.current_uri_changed.connect(on_current_uri_changed);
		on_current_uri_changed(internal_navigation.get_current_uri());
		transaction_log.transaction_ended.connect(on_transaction_ended);
		state = "idle";
		task_id = task_service.add_task_object(this);
	}
	
	~Autoredirect(){
		internal_navigation.current_uri_changed.disconnect(on_current_uri_changed);
		transaction_log.transaction_ended.disconnect(on_transaction_ended);
	}
	
	private void on_transaction_ended(string transaction_id){
		if (transaction_log.get_transaction_uri(transaction_id) == this.page_uri && state == "idle") {
			var transaction_state = transaction_log.get_transaction_state(transaction_id);
			if (transaction_state.key == "redirect") {
				string? redirect_uri = null;
				transaction_log.foreach_transaction_log_entry(transaction_id, (entry) => {
					if (entry.topic == "redirect" && entry.uri != null) {
						if (redirect_uri == null) {
							redirect_uri = entry.uri;
						} else {
							return;
						}
					}
				});
				if (redirect_uri != null) {
					if (internal_navigation.may_autoredirect_to(redirect_uri)) {
						internal_navigation.redirect(redirect_uri, PAGE_PLUGIN_ID);
					}
				}
			}
		}
	}
	
	private void on_current_uri_changed(string uri){
		if (this.page_uri != uri) {
			this.page_uri = uri;
		}
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Task //
	/////////////////////////////////////////////
	
	public string get_task_name(){
		return PAGE_PLUGIN_ID;
	}
	
	public string get_task_category(){
		return "navigation";
	}
	
	public double? get_task_progress(){
		return null;
	}
	
	public string get_task_state(){
		return this.state;
	}
	
	public bool has_task_finished(){
		return state == "initalizing";
	}
	
	public string[] get_available_task_actions(){
		if (state == "idle") {
			return {"pause"};
		} else if (state == "paused") {
			return {"unpause"};
		}
		return {};
	}
	
	public bool activate_task_action(string action){
		if (state == "idle" && action == "pause") {
			state = "paused";
			return false;
		} else if (state == "paused" && action == "unpause") {
			state = "idle";
			return false;
		}
		return false;
	}
	
	public void foreach_task_property(Func<string> cb){
		cb("page_uri");
	}
	
	public string? read_task_property(string key){
		switch(key) {
			case "page_uri":
				return page_uri;
			default:
				return null;
		}
	}
	
}

public class Slate.Page.Plugin.GeminiInputToDocument : Slate.Interface.Page.Service.Tasks.Task, Object {
	
	public const string PAGE_PLUGIN_ID = "slate.gemini_input_to_document";
	public const string VERSION = "builtin";
	
	private string page_uri = "";
	private Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	private Slate.Interface.Page.Service.Tasks.Service task_service;
	private Slate.Interface.Page.Service.Document.Store document_store_service;
	private Slate.Interface.Page.Service.Transaction.Log transaction_log;
	private string state = "initalizing";
	private string? task_id = null;
	
	public GeminiInputToDocument(Slate.Interface.Page.Service.Tasks.Service task_service, Slate.Interface.Page.Service.InternalNavigation internal_navigation, Slate.Interface.Page.Service.Transaction.Log transaction_log, Slate.Interface.Page.Service.Document.Store document_store_service){
		this.document_store_service = document_store_service;
		this.transaction_log = transaction_log;
		this.internal_navigation = internal_navigation;
		this.task_service = task_service;
		// hook signals
		internal_navigation.current_uri_changed.connect(on_current_uri_changed);
		on_current_uri_changed(internal_navigation.get_current_uri());
		transaction_log.transaction_ended.connect(on_transaction_ended);
		state = "idle";
		task_id = task_service.add_task_object(this);
	}
	
	~GeminiInputToDocument(){
		internal_navigation.current_uri_changed.disconnect(on_current_uri_changed);
		transaction_log.transaction_ended.disconnect(on_transaction_ended);
	}
	
	private void on_transaction_ended(string transaction_id){
		if (transaction_log.get_transaction_uri(transaction_id) == this.page_uri) {
			remove_document();
			string? uri = transaction_log.get_transaction_uri(transaction_id);
			if (uri == null) {
				return;
			}
			string? message = null;
			transaction_log.foreach_transaction_log_entry(transaction_id, (entry) => {
				if (entry.statuscode != null) {
					if (entry.topic == "state" && entry.key == "input" && entry.protocol == "gemini" && entry.statuscode.has_prefix("1")) {
						message = entry.message;
					}
				}
			});
			if (message != null) {
				var puri = new Slate.Util.Uri(uri);
				puri.index = null;
				puri.query = "{{{_}}}";
				var document = new Slate.Document.SlateDocument.Model.Node.SimpleItem(puri.uri, message);
				document.set_category("form");
				document_store_service.store_document("slate.gemini_input_to_document.main", document);
			}
		}
	}
	
	private void remove_document(){
		document_store_service.remove_document("slate.gemini_input_to_document.main");
		state = "idle";
		this.on_task_state_updated(this);
		this.on_actions_updated(this);
	}
	
	private void on_current_uri_changed(string uri){
		if (this.page_uri != uri) {
			remove_document();
			this.page_uri = uri;
		}
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Task //
	/////////////////////////////////////////////
	
	public string get_task_name(){
		return PAGE_PLUGIN_ID;
	}
	
	public string get_task_category(){
		return "ui";
	}
	
	public double? get_task_progress(){
		return null;
	}
	
	public string get_task_state(){
		return this.state;
	}
	
	public bool has_task_finished(){
		return state == "initalizing";
	}
	
	public string[] get_available_task_actions(){
		if (state == "document_active"){
			return {"dismiss"};
		}
		return {};
	}
	
	public bool activate_task_action(string action){
		if (state == "document_active" && action == "dismiss"){
			remove_document();
			return false;
		}
		return false;
	}
	
	public void foreach_task_property(Func<string> cb){
		cb("page_uri");
	}
	
	public string? read_task_property(string key){
		switch(key) {
			case "page_uri":
				return page_uri;
			default:
				return null;
		}
	}
	
}

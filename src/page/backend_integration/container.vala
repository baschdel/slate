public class Slate.Page.BackendIntegration.Container : Object, Slate.Interface.Page.Container  {
	
	public string landing_page_address = "about:blank";
	public Slate.Interface.Settings.Provider? settings = null;
	public Slate.Interface.Page.Service.PagePlugin.Index? page_plugin_index = null;
	public string[] default_page_plugins = {};
	
	private Slate.Interface.Session session;
	
	public Container(Slate.Interface.Session session){
		this.session = session;
	}
	
	  ////////////////////////////////////
	 // Slate.Interface.Page.Container //
	////////////////////////////////////
	
	public string get_container_name(){
		return session.get_name();
	}
	
	public string get_container_landing_page_address(){
		return landing_page_address;
	}
	
	public bool can_construct_tab_for(string address){
		return true;
	}
	
	public Slate.Interface.Page.Tab? construct_tab(string address, Slate.Interface.Page.Service.Containers? containers_service = null, string? own_container_id = null){
		var loader = new Slate.Page.BackendIntegration.Loader(session);
		return new Slate.Page.Tab(address, loader, settings, containers_service, own_container_id, page_plugin_index, default_page_plugins);
	}
}

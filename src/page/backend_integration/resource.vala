public class Slate.Page.BackendIntegration.Resource : Object, Slate.Interface.Page.Service.Resource.Resource {

	private Slate.Resource resource;
	private string resource_user_id = "backend_integration_"+GLib.Uuid.string_random();
	
	public Resource(Slate.Resource resource){
		this.resource = resource;
		this.resource.increment_users(resource_user_id);
	}
	
	~Resource(){
		this.resource.decrement_users(resource_user_id);
	}

	  //////////////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Resource.Resource //
	//////////////////////////////////////////////////////////
	
	public bool is_available(){
		var file = File.new_for_path(this.resource.filepath);
		return file.query_exists();
	}
	
	// Resource metadata must be static.
	// If the metadata changes submit a different resource
	// Changing metadata is best queryed using a dedicated service
	public string? get_metadata(string key){
		switch(key){
			case "mimetype":
				return this.resource.mimetype;
			case "uri":
				return this.resource.uri;
			default:
				return null;
		}
	}
	
	public void foreach_metadata_key(Func<string> cb){
		cb("mimetype");
		cb("uri");
	}
	
	public bool is_multiuse(){
		return true;
	}
	
	public InputStream? get_resource_content_stream(){
		var file = File.new_for_path(this.resource.filepath);
		try {
			return file.read();
		} catch (Error e){
			print(@"[backend_integration.resource] Error while reading file $(resource.filepath): $(e.message)");
		}
		return null;
	}

}

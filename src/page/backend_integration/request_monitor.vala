public class Slate.Page.BackendIntegration.RequestMonitor : Slate.Interface.Page.Service.Tasks.Task, Object {
	
	private Slate.Request request;
	private Slate.Interface.Page.Service.Transaction.Log transaction_log;
	public string transaction_id { private set; get; }
	private string? resource_id = null;
	private bool is_loading = false;

	private string current_task_state = "waiting";
	private bool task_finished = false;
	
	public RequestMonitor(Slate.Request request, Slate.Interface.Page.Service.Transaction.Log transaction_log){
		this.request = request;
		this.transaction_log = transaction_log;
		this.transaction_id = transaction_log.start_transaction(request.uri, "download", this);
		this.request.status_changed.connect(on_status_update);
		this.request.resource_changed.connect(on_resource_changed);
		this.request.finished.connect(on_transaction_ended);
		this.on_status_update(request);
		if (request.resource != null){
			this.on_resource_changed(request);
		}
	}
	
	~RequestMonitor(){
		this.transaction_log.end_transaction(request.uri);
		unhook();
		debug("Deconstructing monitor\n");
	}
	
	private void unhook(){
		this.request.status_changed.disconnect(on_status_update);
		this.request.resource_changed.disconnect(on_resource_changed);
		this.request.finished.disconnect(on_transaction_ended);
	}
	
	private void on_status_update(Slate.Request request){
		debug(@"on_status_update\n\tstatus: $(request.status)\n\tsubstatus: $(request.substatus)\n");
		string protocol = "unknown";
		string? statuscode = null;
		if (statuscode == null) {
			statuscode = request.arguments.get("gemini.statuscode");
			if (statuscode != null) {
				protocol = "gemini";
			}
		}
		if (request.status.has_prefix("error")) {
			string message = request.substatus;
			if (message == "") {
				message = request.status;
			}
			transaction_log.submit_log_entry(transaction_id, new Slate.Interface.Page.Service.Transaction.LogEntry("error", "message", protocol, "unknown", statuscode, message));
		}
		if ((request.status == "loading") != this.is_loading) {
			this.is_loading = (request.status == "loading");
			if (this.is_loading) {
				this.request.notify["substatus"].connect(on_load_status_updated);
			} else {
				this.request.notify["substatus"].disconnect(on_load_status_updated);
			}
		}
		if (request.status.has_prefix("redirect")) {
			string redirect_type = "temporary";
			if (request.status == "redirect/permanent") {
				redirect_type = "permanent";
			}
			transaction_log.submit_log_entry(transaction_id, new Slate.Interface.Page.Service.Transaction.LogEntry("redirect", redirect_type, protocol, "unknown", statuscode, null, request.substatus));
		}
		if (request.status == "success") {
			if (resource_id != null) {
				transaction_log.submit_log_entry(transaction_id, new Slate.Interface.Page.Service.Transaction.LogEntry("resource", "success", protocol, "unknown", statuscode, null, request.uri, resource_id));
			}
		}
		string status = request.status.split("/",2)[0];
		if (status == "loading") {
			if (request.substatus == "") {
				status = "shaking_hands";
			} else {
				status = "downloading";
			}
		}
		// when cancelled and a partly downloaded resource is available the resource will be submitted with a success
		// we don't want a cancelled to turn into anything
		if (status != current_task_state && current_task_state != "cancelled") {
			transaction_log.submit_log_entry(transaction_id, new Slate.Interface.Page.Service.Transaction.LogEntry("state", status, protocol, "unknown", statuscode, request.substatus));
			this.on_task_state_updated(this);
			current_task_state = status;
			this.on_actions_updated(this);
		}
		this.on_progress_updated(this);
	}
	
	private void on_load_status_updated(){
		this.acquire_resource_id();
		if (this.resource_id != null && this.is_loading) {
			uint64 bytes = 0;
			if (Slate.Util.Intparser.try_parse_base_16_unsigned(request.substatus,out bytes)) {
				transaction_log.update_resource_size(this.resource_id, bytes);
			}
		}
	}
	
	private void on_resource_changed(Slate.Request request){
		var page_resource = new Slate.Page.BackendIntegration.Resource(request.resource);
		this.acquire_resource_id();
		if (this.resource_id != null) {
			this.transaction_log.add_resource(resource_id, page_resource);
		}
	}
	
	private void on_transaction_ended(Slate.Request request){
		this.transaction_log.end_transaction(transaction_id);
		task_finished = true;
		this.on_actions_updated(this);
		this.on_progress_updated(this);
		unhook();
	}
	
	private void acquire_resource_id(uint64? final_size =null){
		lock(this.resource_id){
			if (this.resource_id == null){
				this.resource_id = transaction_log.submit_resource(transaction_id, null, request.uri, null);
			}
		}
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Task //
	/////////////////////////////////////////////
	
	public string get_task_name(){
		return "dragonstone-legacy-loader";
	}
	
	public string get_task_category(){
		return "loader";
	}
	
	public double? get_task_progress(){
		return null;
	}
	
	public string get_task_state(){
		return current_task_state;
	}
	
	public bool has_task_finished(){
		return task_finished;
	}
	
	public string[] get_available_task_actions(){
		if (current_task_state.has_suffix("loading")) {
			return {};
		} else {
			return {"cancel"};
		}
	}
	
	public bool activate_task_action(string action){
		if (has_task_finished()) {
			return false;
		}
		switch (action) {
			case "cancel":
				request.cancel();
				return true;
			default:
				return false;
		}
	}
	
	public void foreach_task_property(Func<string> cb){
		cb("uri");
		cb("reload");
		cb("transaction_id");
		cb("resource_id");
	}
	
	public string? read_task_property(string key){
		switch(key) {
			case "uri":
				return request.uri;
			case "reload":
				return @"$(request.reload)";
			case "transaction_id":
				return transaction_id;
			case "resource_id":
				return resource_id;
			default:
				return null;
		}
	}
}

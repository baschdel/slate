public class Slate.Page.BackendIntegration.Loader : Object, Slate.Interface.Page.Service.Resource.Loader {
	
	private Slate.Interface.Session session;
	
	public Loader(Slate.Interface.Session session){
		this.session = session;
	}
	
	  //////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Resource.Loader //
	//////////////////////////////////////////////////
	
	public string? load_resource(string uri, bool reload, Slate.Interface.Page.Service.Transaction.Log transaction_log){
		var request = session.make_download_request(uri, reload);
		var monitor = new Slate.Page.BackendIntegration.RequestMonitor(request, transaction_log);
		print(@"[backend_integration] loading $uri reload $reload transaction_id $(monitor.transaction_id)\n");
		return monitor.transaction_id;
	}
	
}

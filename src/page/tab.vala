public class Slate.Page.Tab : Object, Slate.Interface.Page.Tab, Slate.Interface.Page.Service.ExternalNavigation, Slate.Interface.Page.Service.LinearHistory {
	
	protected Slate.Interface.Page.Page? page = null;
	protected Slate.Interface.Settings.Provider? persistant_settings = null;
	protected Slate.Interface.Page.Service.Resource.Loader resource_loader;
	protected Slate.Interface.Page.Service.Containers? containers_service = null;
	protected Slate.Interface.Page.Service.PagePlugin.Index? page_plugin_index = null;
	protected string[] default_page_plugins;
	protected string? own_container_id = null;
	protected Slate.Util.Stack<Slate.Page.Service.LinearHistory.Entry> history = new Slate.Util.Stack<Slate.Page.Service.LinearHistory.Entry>();
	protected Slate.Util.Stack<Slate.Page.Service.LinearHistory.Entry> future = new Slate.Util.Stack<Slate.Page.Service.LinearHistory.Entry>();
	
	public Tab(string uri, Slate.Interface.Page.Service.Resource.Loader resource_loader, Slate.Interface.Settings.Provider? persistant_settings = null, Slate.Interface.Page.Service.Containers? containers_service = null, string? own_container_id = null, Slate.Interface.Page.Service.PagePlugin.Index? page_plugin_index = null, string[] default_page_plugins = {}){
		this.resource_loader = resource_loader;
		this.persistant_settings = persistant_settings;
		this.containers_service = containers_service;
		this.own_container_id = own_container_id;
		this.page_plugin_index = page_plugin_index;
		this.default_page_plugins = default_page_plugins;
		go_to_uri(uri, "tab");
	}
	
	private Slate.Page.Service.LinearHistory.Entry get_history_entry_for_current_page(){
		string history_uri = page.get_internal_navigation_service().get_current_uri();
		var syncronisation_service = page.get_syncronisation_service();
		return new Slate.Page.Service.LinearHistory.Entry(history_uri, syncronisation_service);
	}
	
	private void apply_page(string uri, Slate.Interface.Page.Service.Syncronisation? syncronisation){
		lock (this.page) {
			this.page = new Slate.Page.Page(resource_loader, this, persistant_settings, get_history(), syncronisation, uri, containers_service, own_container_id, page_plugin_index, default_page_plugins);
		}
		this.current_page_changed();
	}
	
	  //////////////////////////////
	 // Slate.Interface.Page.Tab //
	//////////////////////////////
	
	public virtual Slate.Interface.Page.Page get_current_page(){
		return page;
	}
	
	public virtual Slate.Interface.Page.Service.ExternalNavigation get_navigation(){
		return this;
	}
	
	public virtual Slate.Interface.Page.Service.LinearHistory? get_history(){
		return this;
	}
	
	public virtual Slate.Interface.Page.Service.Containers? get_continers_service(){
		return containers_service;
	}
	
	public virtual Slate.Interface.Page.Container? get_own_container(){
		if (containers_service != null && own_container_id != null) {
			return containers_service.get_contatiner(own_container_id);
		} else {
			return null;
		}
	}
	
	public virtual string? get_own_container_id(){
		return own_container_id;
	}
	
	  /////////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.ExternalNavigation //
	/////////////////////////////////////////////////////
	
	public virtual void go_to_uri(string uri, string module_name){
		lock (page) {
			if (page != null){
				history.push(this.get_history_entry_for_current_page());
				future.clear();
			}
			apply_page(uri, null);
		}
	}
	
	  ////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.LinearHistory //
	////////////////////////////////////////////////
	
	public virtual void go_back(){
		lock (page) {
			var history_entry = history.pop();
			if (history_entry != null) {
				future.push(get_history_entry_for_current_page());
				apply_page(history_entry.uri, history_entry.syncronisation);
			}
		}
	}
	
	public virtual bool can_go_back(){
		return history.size() > 0;
	}
	
	public virtual string? get_next_past_uri(){
		var history_entry = history.peek();
		if (history_entry != null) {
			return history_entry.uri;
		} else {
			return null;
		}
	}
	//will iterate over the past uris in reverse cronological order
	public virtual void foreach_past_uri(Func<string> cb){
		foreach(var history_entry in history.list){
			cb(history_entry.uri);
		}
	}
	
	public virtual void go_forward(){
		lock (page) {
			var history_entry = future.pop();
			if (history_entry != null) {
				history.push(get_history_entry_for_current_page());
				apply_page(history_entry.uri, history_entry.syncronisation);
			}
		}
	}
	
	public virtual bool can_go_forward(){
		return future.size() > 0;
	}
	
	public virtual string? get_next_future_uri(){
		var history_entry = future.peek();
		if (history_entry != null) {
			return history_entry.uri;
		} else {
			return null;
		}
	}
	
	// will iterate over the future uris in cronological order
	public virtual void foreach_future_uri(Func<string> cb){
		foreach(var history_entry in future.list){
			cb(history_entry.uri);
		}
	}
}

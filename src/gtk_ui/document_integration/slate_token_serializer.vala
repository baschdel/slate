namespace Slate.GtkUi.DocumentIntegration.SlateTokenSerializer {

	/*
		This namespace contains the functionality needed to pipe a slate document into an adapted token renderer from dragonstone.
	*/
	
	// function that wraps the serializer
	public static void render_node_tree(Slate.Document.SlateDocument.Interface.Node node, Slate.GtkUi.Interface.TokenRenderer renderer){
		//make a decision based on the node type
		var node_type = node.get_node_type();
		if (node_type == SECTION) {
			// if it is a section node, render all the child nodes
			node.foreach_subnode((subnode) => {
				render_node(subnode, renderer, 0, node.get_node_category(), Slate.Util.Uri.maybe(node.get_node_uri()));
			});
		} else {
			// otherwise render the node directly
			render_node(node, renderer);
		}
	}
	
	// function that does the actual serializing from the document to tokens
	private static void render_node(Slate.Document.SlateDocument.Interface.Node node, Slate.GtkUi.Interface.TokenRenderer renderer, uint level = 0, string? parent_category = null, Slate.Util.Uri? base_uri = null){
		/*
			The serializer thakes the following arguments:
			* the node it should render
			* the renderer to append the output to
			* the current recusion and section level
			* the parent node category
		*/
		
		/*
			Gets some variables from the node, so that they are easier to use
		*/
		var node_type = node.get_node_type();
		bool preformatted = node.get_preferred_text_wrap_mode() == PREFORMATTED;
		string? description = node.get_node_description();
		string? text = node.get_node_text();
		string? link_uri = node.get_node_uri();
		string? category = node.get_node_category();
		
		// if we don't have text but a link uri we use that one as our text
		if (text == null) {
			if (link_uri != null) {
				text = link_uri;
			}
		}
		
		if (link_uri != null && base_uri != null) {
			link_uri = Slate.Util.Uri.join(base_uri, new Slate.Util.Uri(link_uri)).uri;
		}
		
		// generate the style class string (has nothing to do with css classes, but works similarly) for the main text
		string style_class;
		// set the main class based on the node type
		switch (node_type) {
			case SECTION:
				style_class = "title";
				break;
			case PARAGRAPH:
				style_class = "paragraph";
				break;
			case ITEM:
				style_class = "link";
				break;
			default:
				// set a dummy class for unknown items and output a warning because that isn't supposed to happen.
				warning("Got an unknown node type!");
				style_class = "thingy";
				break;
		}
		// if we are inside a list …
		if (parent_category == "list") {
			// everything is a list_item
			style_class = "list_item";
		}
		// if we have a link that isn't tagged as a link …
		if (link_uri != null && style_class != "link") {
			// add a styling hint
			style_class += " .link";
		}
		// if we have a category set …
		if (category != null) {
			// turn the category into a styling hint
			style_class += " ."+category;
		}
		
		// append headline/content
		
		// if we have some text
		if (text != null) {
			// start a paragraph tagged with all metadata we just collected
			renderer.start_paragraph(style_class, preformatted, description, level);
			// append the paragraph text and with link uri
			renderer.append_text(text, link_uri);
			// and end the paragraph
			renderer.end_paragraph();
		}
		
		//append children
		
		// if this is a section type node
		if (node_type == SECTION) {
			Slate.Util.Uri uri;
			if (link_uri != null) {
				uri = new Slate.Util.Uri(link_uri);
			} else {
				uri = base_uri;
			}
			// recursively call this function for each subnode, with the own category as parent category and a section level increased by 1
			node.foreach_subnode((subnode) => {
				render_node(subnode, renderer, level+1, category, uri);
			});
		}
	}
	
}

public class Slate.GtkUi.Widget.View.Bar.SlateHeader : Gtk.Box, Slate.GtkUi.Interface.View {
	
	// View related
	private Slate.Interface.Page.Page? page = null;
	private Slate.GtkUi.Interface.ViewContext? context = null;
	private string module_name = "slate_header_view";
	
	private Slate.Ui.NavigationButtonBackend.LinearHistoryBack back_button_backend;
	private Slate.Ui.NavigationButtonBackend.LinearHistoryForward forward_button_backend;
	private Slate.Ui.AddressbarBackend.MainAddressbar address_bar_backend;
	
	// Widgets
	private Slate.GtkUi.Widget.Viewlet.NavigationButton back_button;
	private Slate.GtkUi.Widget.Viewlet.NavigationButton forward_button;
	private Slate.GtkUi.Widget.Viewlet.MainAddressEntry address_entry;
	
	public SlateHeader(){
		
		this.hexpand = true;
		this.homogeneous = false;
		this.halign = Gtk.Align.FILL;
		this.spacing = 6;
		
		back_button_backend = new Slate.Ui.NavigationButtonBackend.LinearHistoryBack();
		forward_button_backend = new Slate.Ui.NavigationButtonBackend.LinearHistoryForward();
		address_bar_backend = new Slate.Ui.AddressbarBackend.MainAddressbar();
		
		back_button = new Slate.GtkUi.Widget.Viewlet.NavigationButton("go-previous-symbolic", "", back_button_backend);
		//back_button.has_frame = false; GTK4
		back_button.relief = NONE;
		back_button.halign = Gtk.Align.START;
		pack_start(back_button, false, false);
		
		forward_button = new Slate.GtkUi.Widget.Viewlet.NavigationButton("go-next-symbolic", "", forward_button_backend);
		//forward_button.has_frame = false; GTK4
		forward_button.relief = NONE;
		forward_button.halign = Gtk.Align.START;
		pack_start(forward_button, false, false);
		
		address_entry = new Slate.GtkUi.Widget.Viewlet.MainAddressEntry(address_bar_backend);
		address_entry.halign = Gtk.Align.FILL;
		pack_start(address_entry, true, true);
		
		//menubutton
		//TODO
		
	}
	
	  ////////////////////////////////
	 // Slate.GtkUi.Interface.View //
	////////////////////////////////
	
	public bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext context, string module_name, string? target_id){
		this.page = page;
		this.context = context;
		this.module_name = module_name;
		address_bar_backend.module_name = module_name+".address_entry";
		reload();
		var translation = context.get_translation();
		back_button.tooltip_label = translation.get_localized_string("slate.history.go_back_button.tooltip_text");
		forward_button.tooltip_label = translation.get_localized_string("slate.history.go_forward_button.tooltip_text");
		address_entry.reload_tooltip_label = translation.get_localized_string("slate.main_address_entry.reload_button.tooltip_text");
		address_entry.go_tooltip_label = translation.get_localized_string("slate.main_address_entry.go_button.tooltip_text");
		return true;
	}
	
	public void reload(){
		if (context == null){
			this.sensitive = false;
		} else {
			this.sensitive = true;
			back_button_backend.page = page;
			back_button.reload();
			forward_button_backend.page = page;
			forward_button.reload();
		}
		address_bar_backend.page = page;
		address_bar_backend.view_context = context;
	}
	
	public string? get_title(){
		return null;
	}
	
}

public class Slate.GtkUi.Widget.View.Bar.SimpleLoading : Gtk.InfoBar, Slate.GtkUi.Interface.View {
	
	// View related
	private Slate.Interface.Page.Page? page = null;
	private Slate.GtkUi.Interface.ViewContext? context = null;
	private string module_name = "slate_header_view";
	private string localized_connecting = "Connecting";
	private string localized_downloading = "Downloading";
	
	public Slate.Ui.LoadingWidgetBackend.WatchTransaction watch_transaction;
	
	private Gtk.Label status_label = new Gtk.Label("status_label");
	private Slate.GtkUi.Widget.Viewlet.LoadingIcon loading_icon;
	private Slate.GtkUi.Widget.Viewlet.SimpleTransactionButtons transaction_buttons;
	
	construct {	
		watch_transaction = new Slate.Ui.LoadingWidgetBackend.WatchTransaction();
		loading_icon = new Slate.GtkUi.Widget.Viewlet.LoadingIcon(watch_transaction);
		transaction_buttons = new Slate.GtkUi.Widget.Viewlet.SimpleTransactionButtons(watch_transaction);
		watch_transaction.progress_updated.connect(on_progress_update);
		var content_box = new Gtk.Box(HORIZONTAL, 4);
		content_box.pack_start(loading_icon, false, false);
		content_box.pack_start(status_label, true, true);
		this.get_content_area().add(content_box);
		this.get_action_area().pack_end(transaction_buttons, false, false);
		this.message_type = INFO;
		this.response.connect((response) => {
			if (response == Gtk.ResponseType.CLOSE){
				this.revealed = false;
			}
		});
		this.show_all();
	}
	
	private void on_progress_update(){
		string text = localized_downloading;
		if (watch_transaction.is_connecting()){
			text = localized_connecting;
		} else {
			double? overall_progress = watch_transaction.get_overall_progress();
			if (overall_progress != null){
				text += @" $((int)(watch_transaction.get_overall_progress()*100))%";
			}
			text += @" ($((int)(watch_transaction.get_bytes_downloaded()/1000))KB";
			uint64? final_size = watch_transaction.get_expected_bytes_downloaded();
			double? current_speed = watch_transaction.get_current_download_speed();
			double? average_speed = watch_transaction.get_average_download_speed();
			if (final_size != null){
				text += @"/$((int)(final_size/1000))KB";
			}
			text += ")";
			if (current_speed != null){
				text += @" $((int)(current_speed/1000))KB/s";
			}
			if (average_speed != null){
				text += @" / $((int)(average_speed/1000))KB/s";
			}
		}
		set_status_label(text);
	}
	
	private void set_status_label(string text){
		Timeout.add(0,() => {
			status_label.label = text;
			return false;
		});
	}
	
	  ////////////////////////////////
	 // Slate.GtkUi.Interface.View //
	////////////////////////////////
	
	public bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext context, string module_name, string? target_id){
		this.page = page;
		this.context = context;
		this.module_name = module_name;
		watch_transaction.page = this.page;
		watch_transaction.set_target(target_id);
		transaction_buttons.update_context(context);
		var translation = context.get_translation();
		localized_connecting = translation.get_localized_string("slate.simple_loading.connecting");
		localized_downloading = translation.get_localized_string("slate.simple_loading.downloading");
		reload();
		return true;
	}
	
	public void reload(){
		if (context == null){
			this.sensitive = false;
		} else {
			this.sensitive = true;
			
		}
	}
	
	public string? get_target_type(){
		return "transaction";
	}
	
	public string? get_target_id(){
		return watch_transaction.get_transaction_id();
	}
	
	public string? get_title(){
		return status_label.label;
	}
}

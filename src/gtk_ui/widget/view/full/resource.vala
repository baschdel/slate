public class Slate.GtkUi.Widget.View.Full.Resource : Gtk.Box, Slate.GtkUi.Interface.View {

	private Slate.GtkUi.Interface.ResourceView resource_viewlet;
	private string? target_id = null;
	private string module_name = "resource_view";
	private Slate.Interface.Page.Service.Resource.Service? resource_service;
	private Slate.GtkUi.Interface.ViewContext? view_context = null;
	private Gtk.Widget? currently_displayed_widget = null;
		
	public Resource(Slate.GtkUi.Interface.ResourceView resource_viewlet){
		this.resource_viewlet = resource_viewlet;
	}
	
	private void set_widget(Gtk.Widget widget, bool center = false){
		if (currently_displayed_widget != null){
			this.remove(currently_displayed_widget);
		}
		currently_displayed_widget = widget;
		if (center) {
			this.set_center_widget(widget);
		} else {
			this.pack_start(widget, true, true);
		}
	}
	
	  ////////////////////////////////
	 // Slate.GtkUi.Interface.View //
	////////////////////////////////
	
	public bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext context, string module_name, string? target_id){
		this.view_context = context;
		if (page != null && target_id != null){
			this.module_name = module_name;
			this.target_id = target_id;			
			this.resource_service = page.get_resource_service();
			this.reload();
			return true;
		}
		return false;
	}
	
	public void reload(){
		if (resource_service != null && target_id != null && view_context != null){
			var resource = resource_service.get_resource(target_id);
			if (resource != null) {
				if (resource_viewlet.could_render_resource(resource)) {
					var input_stream = resource.get_resource_content_stream();
					if (input_stream != null) {
						resource_viewlet.display_resource.begin(resource, input_stream, null, view_context);
						this.set_widget(resource_viewlet);
					} else {
						this.set_widget(new Gtk.Label("---–< bzzt >–---"));	
					}
				} else {
					this.set_widget(new Gtk.Label("???"));
				}
			}
		}
		this.show_all();
	}
	
	public virtual string? get_target_type(){
		return "resource";
	}
	public virtual string? get_target_id(){
		return target_id;
	}
	
	public string? get_title(){
		return resource_viewlet.get_title();
	}
}

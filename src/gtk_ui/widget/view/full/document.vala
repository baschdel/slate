public class Slate.GtkUi.Widget.View.Full.Document : Gtk.Box, Slate.GtkUi.Interface.View {

	private string? target_id = null;
	private string module_name = "document_view";
	private string? title = null;
	private Slate.Interface.Page.Service.Document.Store? document_store_service;
	private Slate.GtkUi.Interface.ViewContext? view_context = null;
	// TODO: load theming related settings from view context
	private Slate.GtkUi.Widget.Hypertext hypertext = new Slate.GtkUi.Widget.Hypertext(null, null);
	private Slate.Interface.Page.Service.ExternalNavigation? external_navigation = null;
	private Slate.Interface.Page.Page? page = null;
	private string? container_id = null;
	
	construct {
		this.pack_start(hypertext);
		this.hypertext.go.connect(go);
		this.show_all();
	}
	
	private void go(string uri, bool alt){
		if (alt) {
			lock (view_context) {
				if (view_context != null) {
					//TODO: use the same sttings as fo the tab this is part of
					view_context.open_uri_in_new_tab(uri, page, container_id);
				}
			}
		} else {
			lock (external_navigation) {
				if (external_navigation != null) {
					external_navigation.go_to_uri(uri, module_name);
				}
			}
		}
	}
	
	  ////////////////////////////////
	 // Slate.GtkUi.Interface.View //
	////////////////////////////////
	
	public bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext context, string module_name, string? target_id){
		if (page != null && target_id != null){
			lock (view_context) {
				this.view_context = context;
			}
			this.page = page;
			if (page != null) {
				this.container_id = page.get_own_container_id();
			} else {
				this.container_id = null;
			}
			this.module_name = module_name;
			this.target_id = target_id;			
			this.document_store_service = page.get_document_store_service();
			lock (external_navigation) {
				this.external_navigation = page.get_external_navigation_service();
			}
			hypertext.link_icon_provider = view_context.get_link_icon_provider();
			this.reload();
			return true;
		}
		return false;
	}
	
	public void reload(){
		if (document_store_service != null && target_id != null && view_context != null){
			var document = document_store_service.retrieve_document(target_id);
			if (document != null) {
				title = document.get_node_text();
				hypertext.reset_renderer();
				Slate.GtkUi.DocumentIntegration.SlateTokenSerializer.render_node_tree(document, hypertext);
			}
		}
		this.show_all();
	}
	
	public virtual string? get_target_type(){
		return "document";
	}
	
	public virtual string? get_target_id(){
		return target_id;
	}
	
	public string? get_title(){
		return title;
	}
}

public class Slate.GtkUi.Widget.Window : Gtk.ApplicationWindow {
	
	private Slate.Interface.Localization.Translation translation;
	private Slate.GtkUi.Interface.ViewContext application_view_context;
	private Slate.GtkUi.ViewContext.Window window_view_context;
	private Slate.Interface.Page.Page? current_page = null;
	private Slate.Interface.Page.Tab? current_tab = null;
	private Slate.GtkUi.Interface.PageFrame? current_page_frame = null;
	private Slate.Interface.Page.Service.Containers containers_service;
	private Slate.Interface.Core core;
	
	public string default_container = "default";
	
	// Widgets
	private Gtk.Notebook tabs;
	private Gtk.HeaderBar header_bar;
	private Slate.GtkUi.Interface.View header_view;
	
	public Window(Gtk.Application application, Slate.GtkUi.Interface.ViewContext view_context, Slate.Interface.Core core) {
		this.application = application;
		this.application_view_context = view_context;
		this.core = core;
		this.initalize();
		this.show();
	}
	
	public Window.from_dropped_tab(Gtk.Application application, Slate.GtkUi.Interface.ViewContext view_context, Slate.Interface.Core core) {
		this.application = application;
		this.application_view_context = view_context;
		this.core = core;
		this.initalize();
		this.show();
	}
	
	public void open_uri_in_new_tab(string uri, string? target_container){
		print("open_uri_in_new_tab "+uri+"\n");
		Slate.Interface.Page.Container container = null;
		string container_id = "";
		if (target_container != null) {
			container = containers_service.get_contatiner(target_container);
			container_id = target_container;
		}
		if (container == null) {
			container = containers_service.get_contatiner(this.default_container);
			container_id = this.default_container;
		}
		if (container != null) {
			var tab = container.construct_tab(uri, this.containers_service, container_id);
			if (tab != null) {
				this.add_page_frame(new Slate.GtkUi.Widget.PageFrame(tab, window_view_context));
			}
		}
	}
	
	public void close_tab(Gtk.Widget? _page_frame){
		int page_num = -1;
		var page_frame = _page_frame;
		if (page_frame != null) {
			page_num = tabs.page_num(page_frame);
		} else {
			page_num = tabs.get_current_page();
			page_frame = tabs.get_nth_page(page_num);
		}
		if (page_num < 0) { return; }
		tabs.remove_page(page_num);
	}
	
	 /////////////////////////////
	// Private stuff
	
	private void initalize() {
		set_default_size(800,600);
		
		this.containers_service = core.get_containers_service();
		this.window_view_context = new Slate.GtkUi.ViewContext.Window(this, this.application_view_context);
		this.translation = window_view_context.get_translation();
		
		/* GTK4
		close_request.connect( e => {
			return before_destroy();
		});
		*/
		
		//initalize header bar
		this.header_bar = new Gtk.HeaderBar();
		this.set_titlebar(header_bar);
		this.set_header_view(new Slate.GtkUi.Widget.View.Bar.SlateHeader());
		
		// tabs
		tabs = new Gtk.Notebook();
		tabs.scrollable = true;
		tabs.set_group_name("slate.tabs");
		
		tabs.page_added.connect((widget, pagenum) => {
			if (widget is Slate.GtkUi.Interface.PageFrame){
				var page_frame = (widget as Slate.GtkUi.Interface.PageFrame);
				page_frame.set_global_view_context(window_view_context);
			}
		});
		
		tabs.create_window.connect((page,x,y) => {
			print("Creating new window…\n");
			var window = new Slate.GtkUi.Widget.Window.from_dropped_tab(this.application, application_view_context, core);
			window.move(x,y);
			return window.tabs;
		});
		
		tabs.switch_page.connect((page, page_num) => {
			this.set_current_page_frame(page as Slate.GtkUi.Interface.PageFrame);
		});
		
		tabs.page_removed.connect((page, page_num) => {
			if (tabs.get_n_pages() == 0){
				this.set_current_page_frame(null);
			}
		});
		
		/*
			Add a new tab button wich for now can only create tabs for the default session
		*/
		//var add_button = new Gtk.Button.from_icon_name("tab-new-symbolic");
		var add_button = new Gtk.MenuButton();
		add_button.popover = get_new_tab_menu_popover(add_button);
		add_button.image = new Gtk.Image.from_icon_name("tab-new-symbolic", BUTTON);
		//add_button.has_frame = false; GTK4
		add_button.relief = NONE;
		/*add_button.clicked.connect(() => {
			open_uri_in_new_tab("about:blank", null);
		});*/
		tabs.set_action_widget(add_button,Gtk.PackType.END);
		add_button.show();
		this.child = tabs;
		this.show_all();
		
	}
	
	private Gtk.Popover get_new_tab_menu_popover(Gtk.Widget? relative_to){
		var popover = new Gtk.Popover(relative_to);
		new Slate.GtkUi.Widget.Helper.MenuButton("");
		var box = new Gtk.Box(VERTICAL, 3);
		box.margin = 4;
		containers_service.foreach_container((name) => {
			var container = containers_service.get_contatiner(name);
			if (container != null) {
				var uri = container.get_container_landing_page_address();
				var button = new Slate.GtkUi.Widget.Helper.MenuButton(name);
				button.clicked.connect(() => {
					popover.popdown();
					open_uri_in_new_tab(uri, name);
				});
				button.show();
				box.pack_start(button);
			}
		});
		box.show_all();
		popover.add(box);
		return popover;
	}
	
	private void add_page_frame(Slate.GtkUi.Interface.PageFrame page_frame){
		page_frame.show();
		tabs.append_page(page_frame, new Slate.GtkUi.Widget.TabHeader(page_frame, this, translation));
		tabs.set_tab_reorderable(page_frame, true);
		tabs.set_tab_detachable(page_frame, true);
		tabs.set_current_page(-1);
	}
	
	private void set_current_page_frame(Slate.GtkUi.Interface.PageFrame? page_frame){
		this.current_page_frame = page_frame;
		if (current_page_frame != null){
			set_current_tab(current_page_frame.get_tab());
		} else {
			set_current_tab(null);
		}
	}
	
	private void set_current_tab(Slate.Interface.Page.Tab? tab){
		if (current_tab != null) {
			current_tab.current_page_changed.disconnect(on_page_changed);
		}
		this.current_tab = tab;
		if (current_tab != null) {
			current_tab.current_page_changed.connect(on_page_changed);
		}
		on_page_changed();
	}
	
	private void on_page_changed(){
		if (current_tab != null){
			set_current_page(this.current_tab.get_current_page());
		} else {
			set_current_page(null);
		}
	}
	
	private void set_current_page(Slate.Interface.Page.Page? page){
		this.current_page = page;
		update_header_page();
	}
	
	private void update_header_page(){
		header_view.display_page(current_page, window_view_context, "main_header_bar", null);
		header_view.reload();
	}
	
	private void set_header_view(Slate.GtkUi.Interface.View header_view){
		this.header_view = header_view;
		//header_bar.title_widget = header_view; GTK4
		header_bar.custom_title = header_view;
		update_header_page();
	}
	
	private void close_all_tabs(){
		while(tabs.get_n_pages()>0){
			tabs.remove_page(0);
		}
	}
	
	public bool before_destroy() {
		this.close_all_tabs();
		return false;
	}
	
}

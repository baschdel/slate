public class Slate.GtkUi.Widget.TabHeader : Gtk.Box {

	private Slate.GtkUi.Widget.Window window;
	private Slate.GtkUi.Interface.PageFrame page_frame;
	private Slate.Interface.Page.Tab tab;
	private Slate.Interface.Page.Page page;
	private Slate.Interface.Page.Service.InternalNavigation? internal_navigation = null;
	private Slate.Interface.Localization.Translation translation;
	
	//Widgets
	private Gtk.Button close_button;
	private Gtk.Label title = new Gtk.Label("💫️");
	private int title_chars = 25;
	
	public TabHeader(Slate.GtkUi.Interface.PageFrame page_frame, Slate.GtkUi.Widget.Window window, Slate.Interface.Localization.Translation translation){
		this.page_frame = page_frame;
		this.window = window;
		this.translation = translation;
		this.tab = page_frame.get_tab();
		this.tab.current_page_changed.connect(on_page_changed);
		close_button = new Gtk.Button.from_icon_name("window-close-symbolic");
		//close_button.has_frame = false; //GTK4
		close_button.relief = NONE;
		close_button.tooltip_text = translation.get_localized_string("slate.tab_header.close_button.tooltip_text");
		this.orientation = Gtk.Orientation.HORIZONTAL;
		pack_start(title, true, true);
		pack_start(close_button, false, false);
		close_button.clicked.connect(() => {
			window.close_tab(page_frame);
		});
		on_page_changed();
		this.show_all();
	}
	
	~TabHeader(){
		this.tab.current_page_changed.disconnect(on_page_changed);
		if (internal_navigation != null){
			internal_navigation.current_uri_changed.disconnect(on_uri_changed);
		}
	}
	
	private void on_page_changed(){
		if (internal_navigation != null){
			internal_navigation.current_uri_changed.disconnect(on_uri_changed);
		}
		this.page = tab.get_current_page();
		this.internal_navigation = this.page.get_internal_navigation_service();
		if (internal_navigation != null){
			internal_navigation.current_uri_changed.connect(on_uri_changed);
		}
		this.update_title();
	}
	private void on_uri_changed(string uri){
		this.update_title();
	}
	
	private void update_title(){
		string? uri = null;
		if (internal_navigation != null){
			uri = internal_navigation.get_current_uri();
		}
		if (uri == null) {
			uri = "<unknown_uri>";
		}
		//TODO: Try to get the actual title from the tab metadata once that's implemented
		string title = uri;
		if (title.char_count() > title_chars){
			if (title == uri){
				var startcut = title.index_of_nth_char(title_chars/2);
				var endcut = title.index_of_nth_char(title.char_count()-(title_chars/2));
				this.title.label = title[0:startcut]+"…"+title.slice(endcut,title.length);
			} else {
				var cutat = title.index_of_nth_char(title_chars);
				this.title.label = title[0:cutat]+"…";
			}
		} else {
			this.title.label = title;
		}
		this.tooltip_text = uri;
	}
}

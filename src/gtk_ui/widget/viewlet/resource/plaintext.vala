public class Slate.GtkUi.Widget.Viewlet.Resource.PlainText : Slate.GtkUi.Interface.ResourceView, Gtk.ScrolledWindow {
	
	private Gtk.TextView textview;
	private string? title = null;
	private string? error_message = null;
	private bool is_loading = true;
	
	public PlainText(){
		this.textview = new Gtk.TextView();
		this.add(textview);
		textview.wrap_mode = WORD_CHAR;
		textview.monospace = true;
	}
	
	~PlainText(){
	}
	
	private void on_error(string message){
		this.error_message = message;
		this.on_load_error(message);
	}
	
	private void on_finished(){
		is_loading = false;
		this.on_load_finished();
	}
	
	  ////////////////////////////////////////
	 // Slate.GtkUi.Interface.ResourceView //
	////////////////////////////////////////
	
	public bool could_render_resource(Slate.Interface.Page.Service.Resource.Resource resource){
		var mimetype = resource.get_metadata("mimetype");
		if (mimetype != null){
			if (! (mimetype.has_prefix("text/") || mimetype.has_prefix("application"))) {
				return false;
			}
		}
		return true;
	}
	
	public async void display_resource(Slate.Interface.Page.Service.Resource.Resource resource, InputStream input_stream, Cancellable? cancellable, Slate.GtkUi.Interface.ViewContext context){
		try {
			var buffer = textview.buffer;
			buffer.text = "";
			Gtk.TextIter end_iter;
			buffer.get_end_iter(out end_iter);
			var data_stream = new DataInputStream(input_stream);
			string line = null;
			while ((line = yield data_stream.read_line_async(Priority.DEFAULT)) != null){
				if (line.validate(line.length)) {
					if (title == null) {
						string stripped_line = line.strip();
						if (stripped_line != "" && stripped_line.char_count() < 64){
							this.title = stripped_line;
						}
					}
					buffer.insert(ref end_iter, line, line.length);
					buffer.insert(ref end_iter, "\n", -1);
				}
			}
		} catch (Error e) {
			on_error(e.message);
		}
		on_finished();
	}
	
	public bool is_loading_resource(){
		return this.is_loading;
	}
	
	public string? get_latest_error_message(){
		return this.error_message;
	}
	
	public string? get_title(){
		return this.title;
	}
	
}

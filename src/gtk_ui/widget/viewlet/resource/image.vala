public class Slate.GtkUi.Widget.Viewlet.Resource.Image : Slate.GtkUi.Interface.ResourceView, Gtk.ScrolledWindow {
	
	private string? title = null;
	private string? error_message = null;
	private bool is_loading = true;
	
	private Gtk.Image image;
	private Gdk.Pixbuf? pixbuf = null;
	
	public float factor = 1;
	public bool autoscale = true;
	
	public Image(){
		this.image = new Gtk.Image();
		add(image);
		size_allocate.connect(trigger_autoscale);
		set_events(Gdk.EventMask.ALL_EVENTS_MASK);
		this.scroll_event.connect((event) => {
			if((event.state & Gdk.ModifierType.CONTROL_MASK) > 0){
				float new_factor = factor-((float) event.delta_y)/10;
				if (new_factor > 0 && new_factor < 10){
					scale(new_factor);
				}
				autoscale = false;
				return true;
			}
			return false;
		});
		show_all();
	}
	
	private void on_error(string message){
		this.error_message = message;
		this.on_load_error(message);
	}
	
	private void on_finished(){
		is_loading = false;
		this.on_load_finished();
	}
	
	public void trigger_autoscale(Gtk.Allocation rect){
		if(autoscale){
			scale_to_window(false);			
		}
	}
	
	public void scale_to_window(bool do_not_magnify = true){
		if (pixbuf != null) {
			float ph = pixbuf.get_height();
			float pw = pixbuf.get_width();
			float wh = get_allocated_height();
			float ww = get_allocated_width();
			float hr = ph/wh;
			float wr = pw/ww;
			float new_factor = factor;
			if (hr > 1 || wr > 1 || !do_not_magnify){
				new_factor = 1/float.max(hr,wr);
				//print(@"[image] rect.width=$(rect.width) rect.height=$(rect.height)\n");
				//print(@"[image] width: $pw,$ww rat: $wr | height: $ph,$wh rat: $hr\n");
				//print(@"[image] factor: $factor\n");
			}
			scale(new_factor);
		}
	}
	
	public void scale(float factor){
		if (pixbuf != null) {
			float off = this.factor/factor;
			this.factor = factor;
			int ph = pixbuf.get_height();
			int pw = pixbuf.get_width();
			int w = (int) (pw*factor);
			int h = (int) (ph*factor);
			if ((off > 1.0001 || off < 0.9999) && w > 0 && h > 0 && w < 25000 && h < 25000){
				var scaled_pixbuf = pixbuf.scale_simple(w, h, Gdk.InterpType.BILINEAR);
				if (scaled_pixbuf != null){
					image.set_from_pixbuf( (owned) scaled_pixbuf);
				}
			}
			image.set_padding(0,0);
		}
	}
	
	  ////////////////////////////////////////
	 // Slate.GtkUi.Interface.ResourceView //
	////////////////////////////////////////
	
	public bool could_render_resource(Slate.Interface.Page.Service.Resource.Resource resource){
		var mimetype = resource.get_metadata("mimetype");
		if (mimetype != null){
			if (! (mimetype.has_prefix("image/"))) {
				return false;
			}
		}
		return true;
	}
	
	public async void display_resource(Slate.Interface.Page.Service.Resource.Resource resource, InputStream input_stream, Cancellable? cancellable, Slate.GtkUi.Interface.ViewContext context){
		try {
			pixbuf = yield new Gdk.Pixbuf.from_stream_async.begin(input_stream);
			image.set_from_pixbuf(pixbuf);
			scale_to_window();
		} catch (Error e) {
			on_error(e.message);
		}
		on_finished();
	}
	
	public bool is_loading_resource(){
		return this.is_loading;
	}
	
	public string? get_latest_error_message(){
		return this.error_message;
	}
	
	public string? get_title(){
		return this.title;
	}
	
}

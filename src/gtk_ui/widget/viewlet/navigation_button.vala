public class Slate.GtkUi.Widget.Viewlet.NavigationButton : Gtk.Button {
	
	private Slate.Interface.Ui.NavigationButtonBackend backend;
	public string tooltip_label;
	
	public NavigationButton(string icon_name, string tooltip_label, Slate.Interface.Ui.NavigationButtonBackend backend){
		//this.set_child(new Gtk.Image.from_icon_name(icon_name));
		this.set_image(new Gtk.Image.from_icon_name(icon_name, BUTTON));
		this.tooltip_label = tooltip_label;
		this.backend = backend;
	}
	
	public void update_tooltip(){
		var location = this.backend.get_primary_location();
		if (location != null) {
			string label = "<b>"+Markup.escape_text(tooltip_label)+"</b>\n";
			if (label == "<b></b>\n") {
				label = "";
			}
			this.set_tooltip_markup(label+Markup.escape_text(location));
		} else {
			this.has_tooltip = false;
		}
	}
	
	public void reload(){
		this.sensitive = backend.can_go();
		update_tooltip();
	}
	
	protected override void clicked(){
		backend.go();
	}
	
}

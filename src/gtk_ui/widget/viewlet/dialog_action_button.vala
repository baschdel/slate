public class Slate.GtkUi.Widget.Viewlet.DialogActionButton : Gtk.Button {
	
	private Slate.Interface.Ui.DialogAction action;
	private string dialog_name;
	
	public DialogActionButton(Slate.Interface.Ui.DialogAction action, string dialog_name, bool is_primary){
		this.action = action;
		this.dialog_name = dialog_name;
		string? label = action.get_action_uri();
		if (label == null){
			label = action.get_action_name();
		}
		this.label = label;
		if (action.is_action_destructive()) {
			this.get_style_context().add_class("destructive-action");
		} else if (is_primary) {
			this.get_style_context().add_class("suggested-action");
		}
	}
	
	protected override void clicked(){
		action.activate_action();
	}
	
	public void update_context(Slate.GtkUi.Interface.ViewContext context){
		var translation = context.get_translation();
		string? label = action.get_action_uri();
		string action_name = action.get_action_name();
		if (label == null){
			label = translation.get_localized_string(@"slate.dialog.$dialog_name.action.$action_name.label");
		}
		this.label = label;
		this.tooltip_text = translation.get_localized_string(@"slate.dialog.$dialog_name.action.$action_name.tooltip_text");
	}
	
}

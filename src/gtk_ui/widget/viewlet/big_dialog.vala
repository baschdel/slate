public class Slate.GtkUi.Widget.Viewlet.BigDialog : Gtk.Box {
	
	private Slate.Interface.Ui.Dialog dialog;
	
	private Gtk.Label title_label;
	private Gtk.Label subtitle_label;
	private Gtk.Label message_label;
	
	public BigDialog(Slate.Interface.Ui.Dialog dialog){
		this.dialog = dialog;
		string dialog_name = dialog.get_dialog_name();
		this.title_label = new Gtk.Label(dialog_name);
		this.subtitle_label = new Gtk.Label(dialog_name);
		this.message_label = new Gtk.Label("");
		title_label.wrap_mode = WORD_CHAR;
		title_label.wrap = true;
		make_label_big_headline(title_label);
		subtitle_label.wrap_mode = WORD_CHAR;
		subtitle_label.wrap = true;
		make_label_small_headline(subtitle_label);
		message_label.selectable = true;
		message_label.wrap_mode = WORD_CHAR;
		message_label.wrap = true;
		this.orientation = VERTICAL;
		this.spacing = 8;
		this.margin = 16;
		this.pack_start(title_label);
		this.pack_start(subtitle_label);
		string? dialog_message = dialog.get_dialog_message();
		if (dialog_message != null) {
			message_label.label = dialog_message;
			this.pack_start(message_label);
		}
		bool is_primary = dialog.has_primary_action();
		foreach (var action in dialog.get_actions()){
			var button = new Slate.GtkUi.Widget.Viewlet.DialogActionButton(action, dialog_name, is_primary);
			this.pack_start(button);
			is_primary = false;
		}
	}
	
	public string get_title(){
		return title_label.label;
	}
	
	private void make_label_small_headline(Gtk.Label label){
		var label_attr_list = new Pango.AttrList();
		label_attr_list.insert(new Pango.AttrSize(16000));
		var label_font_description = new Pango.FontDescription();
		label_font_description.set_style(Pango.Style.OBLIQUE);
		label_attr_list.insert(new Pango.AttrFontDesc(label_font_description));
		label.attributes = label_attr_list;
	}
	
	private void make_label_big_headline(Gtk.Label label){
		var label_attr_list = new Pango.AttrList();
		label_attr_list.insert(new Pango.AttrSize(48000));
		label.attributes = label_attr_list;
	}
	
	public void update_context(Slate.GtkUi.Interface.ViewContext context){
		var translation = context.get_translation();
		string dialog_name = dialog.get_dialog_name();
		title_label.label = translation.get_localized_string(@"slate.dialog.$dialog_name.title");
		subtitle_label.label = translation.get_localized_string(@"slate.dialog.$dialog_name.subtitle");
		this.foreach((widget) => {
			var action_button = widget as Slate.GtkUi.Widget.Viewlet.DialogActionButton;
			if (action_button != null) {
				action_button.update_context(context);
			}
		});
	}
	
}

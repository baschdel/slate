public class Slate.GtkUi.Widget.Viewlet.Tasklist : Gtk.ScrolledWindow {
	
	private Slate.Interface.Page.Service.Tasks.Service tasks_service;
	private Gtk.ListBox list_box = new Gtk.ListBox();
	
	public Tasklist(Slate.Interface.Page.Service.Tasks.Service tasks_service){
		this.tasks_service = tasks_service;
		this.tasks_service.task_added.connect(add_task_signal_handler);
		this.tasks_service.foreach_registred_task(add_task);
		this.add(list_box);
	}
	
	~Tasklist(){
		this.tasks_service.task_added.disconnect(add_task_signal_handler);
	}
	
	private void add_task_signal_handler(string task_id, Slate.Interface.Page.Service.Tasks.Task task){
		Timeout.add(0,() => {
			add_task(task_id, task);
			return false;
		});
	}
	
	private void add_task(string task_id, Slate.Interface.Page.Service.Tasks.Task task){
		var widget = get_task_preview_widget(task_id, task);
		widget.show_all();
		list_box.prepend(widget); //I know this isn't how listboxes are meant to be used …
	}
	
	private Gtk.Widget get_task_preview_widget(string task_id, Slate.Interface.Page.Service.Tasks.Task task){
		//return new Gtk.Label(@"$task_id: $(task.get_task_category()) $(task.get_task_name())\n");
		return new Slate.GtkUi.Widget.Viewlet.Task.Preview(task_id, task);
	}
	
}

public class Slate.GtkUi.Widget.Viewlet.Task.Preview : Gtk.Box {

	private string task_id;
	private Slate.Interface.Page.Service.Tasks.Task task;
	private Gtk.Label task_state_label = new Gtk.Label("TASK STATE UNKNOWN");
	
	public Preview(string task_id, Slate.Interface.Page.Service.Tasks.Task task){
		this.task_id = task_id;
		this.task = task;
		this.orientation = VERTICAL;
		this.pack_start(new Gtk.Label(@"$task_id [$(task.get_task_category())] $(task.get_task_name())"));
		this.pack_start(task_state_label);
		this.update_task_state();
		this.task.on_progress_updated.connect(update_task_state_signal_handler);
		this.task.on_task_state_updated.connect(update_task_state_signal_handler);
	}
	
	~Preview(){
		this.task.on_progress_updated.disconnect(update_task_state_signal_handler);
		this.task.on_task_state_updated.disconnect(update_task_state_signal_handler);
	}
	
	private void update_task_state_signal_handler(Slate.Interface.Page.Service.Tasks.Task task){
		if (task == this.task) {
			Timeout.add(0,() => {
				update_task_state();
				return false;
			});
		}
	}
	
	private void update_task_state(){
		string state_label = task.get_task_state();
		double? progress = task.get_task_progress();
		if (progress != null) {
			state_label += @" ($(GLib.Math.round(progress.clamp(0,1)*1000)/10)%)";
		}
		if (task.has_task_finished()) {
			state_label += " [finished]";
		}
		task_state_label.label = state_label;
	}

}

public class Slate.GtkUi.Widget.Viewlet.SimpleTransactionButtons : Gtk.Box {
	/*
		This Widget provides some selected actions for a transaction
		it is intended to be used when there is not enough space for a lot of transactions.
	*/
	
	private Slate.Interface.Ui.LoadingWidgetBackend backend;
	
	private Gtk.Button cancel_button;
	
	public SimpleTransactionButtons(Slate.Interface.Ui.LoadingWidgetBackend backend){
		this.backend = backend;
		cancel_button = new Gtk.Button.with_label("cancel");
		cancel_button.get_style_context().add_class("destructive-action");
		cancel_button.clicked.connect(trigger_cancel_action);
		this.pack_start(cancel_button);
		backend.transaction_updated.connect(reload);
		backend.resource_updated.connect(reload);
		backend.progress_updated.connect(reload);
		this.reload();
		this.show();
	}
	
	public void update_context(Slate.GtkUi.Interface.ViewContext context){
		var translation = context.get_translation();
		cancel_button.tooltip_text = translation.get_localized_string("slate.simple_transaction_button.cancel.tooltip_text");
		cancel_button.label = translation.get_localized_string("action.cancel");
	}
	
	private void trigger_cancel_action(){
		backend.trigger_action("cancel");
	}
	
	private void reload(){
		bool has_cancel_action = false;
		foreach (string action in backend.get_available_actions()) {
			if (action == "cancel") {
				has_cancel_action = true;
			}
		}
		cancel_button.visible = has_cancel_action;
	}
	
}

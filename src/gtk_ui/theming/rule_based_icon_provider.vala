public class Slate.GtkUi.Theming.RuleBasedIconProvider : Slate.GtkUi.Interface.Theming.LinkIconProvider, Object {

	public string default_icon_name = "go-jump-symbolic";
	public List<Slate.Util.UriPattern> uri_patterns = new List<Slate.Util.UriPattern>();
	
	  ////////////////////////////////////////////////////
	 // Slate.GtkUi.Interface.Theming.LinkIconProvider //
	////////////////////////////////////////////////////
	
	public string get_default_icon_name(){
		return default_icon_name;
	}
	
	public string? get_icon_name_for_uri(string uri){
		var parsed_uri = new Slate.Util.Uri(uri);
		foreach (var pattern in uri_patterns) {
			if (pattern.match_uri(parsed_uri)) {
				return pattern.rule_name;
			}
		}
		return null;
	}
	
	public string? get_icon_name_for_mimetype(string mimetype){
		//TODO: implement mimetype matching patterns
		return null;
	}
}

public interface Slate.GtkUi.Interface.PageFrameLayout : Gtk.Widget {
	
	public abstract void submit_widget(Gtk.Widget widget, string role, string name, string[] hints);
	public abstract void remove_widget(string role);
	public abstract void cleanup();
	
}

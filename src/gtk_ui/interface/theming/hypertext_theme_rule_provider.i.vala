public interface Slate.GtkUi.Interface.HypertextThemeRuleProvider : Object {
	
	public abstract void foreach_relevant_rule(string content_type, string uri, Func<Slate.GtkUi.Theming.HypertextThemeRule> cb);
	
}

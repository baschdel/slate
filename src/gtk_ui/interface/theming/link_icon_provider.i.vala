public interface Slate.GtkUi.Interface.Theming.LinkIconProvider : Object {
		
	/*
		This Interface covers the very basic case of: we have a uri or mimetype, we have no other information about the content, please give me an icon name so that I can give a little hint to the user.
		This is supposed to be supplyed through the view context and used in combination with other link providing services.
		
		This interface provides 3 functions for getting icon names:
		* 1: get_icon_name_for_mimetype(mimetype) - This one returns a specific icon name for a given mimetype, the mimetype may come from a mimetype guess based on the file extension
		* 2: get_icon_name_for_uri(uri) - This one is supposed to return an icon for a specific site and or protocol
		* 3: get_default_icon_name() returns the icon name to use when all other methods return null
		
		Maybe a second uri based match will be introduced for cases where you have to override a mimeguess
	*/
	
	public abstract string get_default_icon_name();
	public abstract string? get_icon_name_for_uri(string uri);
	public abstract string? get_icon_name_for_mimetype(string mimetype);
	
}

public interface Slate.GtkUi.Interface.Theming.HypertextThemeLoader : Object {
	
	public abstract Slate.GtkUi.Interface.Theming.HypertextViewTheme? get_theme_by_name(string name);
	
}

public interface Slate.GtkUi.Interface.View : Gtk.Widget {
	
	// if the page is null blank out or show a default something
	// Usually the only place where you can be and get a null page is the headerbar
	// The target ids meaning depends on the content type this view is showing
	public abstract bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext context, string module_name, string? target_id);
	public abstract void reload();
	
	public virtual string? get_target_type(){ return null; } //dialog, resource, document
	public virtual string? get_target_id(){ return null; }
	
	public abstract string? get_title(); //Use this when switching between multiple views
}

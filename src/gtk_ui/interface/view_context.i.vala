public interface Slate.GtkUi.Interface.ViewContext : Object {
	
	/*
		Instances of this interface are supposed to provide functionality to the
		views that is too specific to gtk to put into the page and can't be solved wwith the configuration.
		
		This includes for example a menu widget factory or an icon provider
	*/
	
	// Interface specific navigation targets
	public abstract void open_uri_in_new_tab(string uri, Slate.Interface.Page.Page? from_page, string? target_container);
	public abstract void open_uri_in_new_window(string uri, Slate.Interface.Page.Page? from_page, string? target_container);
	public abstract Slate.Interface.Localization.Translation get_translation();
	
	// Theming related getters
	
	public abstract Slate.GtkUi.Interface.Theming.LinkIconProvider? get_link_icon_provider();
	
}

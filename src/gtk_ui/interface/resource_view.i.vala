public interface Slate.GtkUi.Interface.ResourceView : Gtk.Widget {
	
	public signal void on_load_finished();
	public signal void on_load_error(string message);
	
	//makes a decisin based on the resources metadata, will not touch the content
	public abstract bool could_render_resource(Slate.Interface.Page.Service.Resource.Resource resource);
	
	// attempt to display the resource, the input_stream will already be opened by the caller to narrow down possible errors,
	// the resource object is there for metadata and in case another input stream is needed for whatever reason.
	// The factory is not allowed to keep any metadata or content or the resulting widgets as global information !
	// The resources will most likely be loaded asyncronously
	public abstract async void display_resource(Slate.Interface.Page.Service.Resource.Resource resource, InputStream input_stream, Cancellable? cancellable, Slate.GtkUi.Interface.ViewContext context);
	
	public abstract bool is_loading_resource();
	public abstract string? get_latest_error_message();
	
	public abstract string? get_title();
}

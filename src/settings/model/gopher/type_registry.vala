public class Slate.Settings.Model.Gopher.TypeRegistry : Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes, Object {
	
	private HashTable<unichar,Slate.Settings.Model.Gopher.TypeRegistryEntry> entrys = new HashTable<unichar,Slate.Settings.Model.Gopher.TypeRegistryEntry>(unichar_hash, unichar_equal);
	
	public static uint unichar_hash(unichar c){
		return ((uint) c)%8192;
	}
	
	public static bool unichar_equal(unichar a, unichar b){
		return a == b;
	}
	
	public TypeRegistry.default_configuration(){
		//fast
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('i',null,".",TEXT));
		//standardized
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('0',"text/*"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('1',"text/gopher"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('2',null,"CCSO://{host}:{port}/{selector}"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('3',null,".",ERROR));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('4',"text/x-hex"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('5',"~application/octet-stream"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('6',null));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('7',"text/gopher","gopher://{host}:{port}/{type}{selector}%09{{{_}}}",SEARCH));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('8',null,"telnet://{host}:{port}"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('9',"~application/octet-stream"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('g',"image/gif"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('I',"image/*"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('T',null,"telnet://{host}:{port}"));
		//gopher+ types (you sometimes find them)
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry(':',"image/*"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry(';',"video/*"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('<',"audio/*"));
		//conventions
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('h',"text/html"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('p',"image/png"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('P',"application/pdf"));
		add(new Slate.Settings.Model.Gopher.TypeRegistryEntry('s',"audio/*"));
	}
	
	public Slate.Settings.Model.Gopher.TypeRegistryEntry? get_entry_by_gophertype(unichar gophertype){
		return entrys.get(gophertype);
	}
	
	public void add(Slate.Settings.Model.Gopher.TypeRegistryEntry entry){
		entrys.set(entry.gophertype,entry);
	}
	
	  ////////////////////////////////////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes //
	////////////////////////////////////////////////////////////////////////////////
	
	public Slate.Document.SlateDocument.Interface.Generator.Configuration.GophertypeClass? get_gophertype_class(unichar gophertype){
		var registry_entry = get_entry_by_gophertype(gophertype);
		if (registry_entry != null) {
			return registry_entry.hint;
		}
		return null;
	}
	
	public string? get_uri(unichar gophertype, string host, string port, string selector){
		var registry_entry = get_entry_by_gophertype(gophertype);
		if (registry_entry != null) {
			return registry_entry.get_uri(host, port, selector);
		}
		return null;
	}
	
}

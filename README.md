# Slate

A very(!) work in progress multiprotocol browser written in Vala.

Please note that core features are still missing!

This one is based on Dragonstone, but with the UI being rewritten.

If you want to adopt this project and make it into your own feel free to do so, I won't be mad if you change the license to a GPL or MIT one.

## Goals and nongoals
In the short then the goal is to reah fetaure parity with dragonstone and a similar look and feel with the UI.
Long therm the UI should become more flexible and maybe even scriptable (unlike the web the scripts come from the client config, NOT from the server)
The client config or the user MUST ALWAYS have the last say over what a page does or how it is displayed, this implies that slate will NEVER support page scripting from server loaded scripts like the modern web does.

Sane defaults: The configuration should provide sane defaults for an unconfigured client, if it does not this should be considered a bug.

Seperate logic and UI: UI elements should contain the minimum amount of logic possible. If a ui element requires some logic delegate that to a seperate class, that would allow rebuilding the same element with a different toolkit with minimal effort.

## How to build/install?
Note: to build slate you need the following dependencies:
- gtk3  - the graphics toolkit
- json-glib - jso parser, used for configutation
- valac - the Vala compiler
- meson - the build system
- cmake - used by meson
- python 3.x - used by meson

To build it, run the `build.sh` script, which will automatically setup
the build folder, run ninja, and put the output in the projects root
directory. The produced binary should be executable, now.

To make development easy, the `run` script calls the `build.sh` script
and then runs whatever is at the output (it will try and fail if there is none).

## Core concepts

### Page
After a lot of thinking and a dozen of scrapped designs (Dragonstone, NightCat and a lot of others, that only exist on paper) I think, that the central element of the browser should be a page.
A page is a collection of modules, that con fetch and parse a document with one root document at a uri.
When a new uri is entered in the address bar a new page is loaded. A page shouldn't navigate, that is handled by a tab.

### Tab (Page)
Tabs are the things that contain pages, the page a tab contains may change.
A tab also provides modules for navigation, persistance and history.
Although currently a tab is coupled to an Ui Element, tabs are independant of them.

### Page Resource (Page)
A page resource is an immutable resource with metadata and an input stream, metadata will become available as the resource loads, but once it is present, it shouldn't change.
The resource also has a getter for an input stream, wich returns a streams for sequential access to the resources data as it becomes avaiable. (Without caching the resource can only give you one stream). This resource is returned after a load request.

### Transaction Resource (Page)
There is also a second resource concept, a transaction resource, wich contains a page resource, but also has some changing meatdata on download progress and speed.

NOTE: There is a second resource concept implemented with the class name Slate.Resource .
This one has a filename and comes from Dragonstone, to reuse its networking infrastructure until that one get's an update.

### Transaction (Page)
A Transaction is a collection of log entrys and Transaction resources and loader configuration, that documents an interaction with the outside world.

### Document (Page)
Documents are not yet Implemented.
A document is the result of parsing a resource. It can be used to extract metadata (like a page title) and as a data source for renderers and other goodies. I have at the time of writing not yet decided how exactly they shouldrepresent information, and whether they should be append only or fully writable.

### Page View (UI)
A page view is the ui through wich a user can interact with the underlying page infrastructure. One page may be displayed by any number (including none) of page views.
A page view only handles one page at a time.

### Page Frame (UI)
A page frame is a collection of one or more pageviews. A pageframe is coupled to a tab and is reponsible for choosing the page views for the current page of the tab.

### View Context (UI)
A view context is UI dependent and is provided by the UI, it contains functionality, that is largely dependent on the UI (like a open in new window action, wich in a terminal based UI would be a bit difficult, or a open in new tab, the page doesn't know how it is displayed, and therefore can't know in wich window the new UI-Tab should appear) 
